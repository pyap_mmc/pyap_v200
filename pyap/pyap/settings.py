﻿import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


SECRET_KEY = 'hph53g@p+i@q0me76#v)+6p&(xcc!)@e=oi8oum)lo_k1_pyap'

DEBUG = True


ALLOWED_HOSTS = ['free-shop.pyap.ru', 'localhost']


INSTALLED_APPS = [
    'suit',
    'filebrowser',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admindocs',
    'django.contrib.sitemaps',
    'mptt',
    'ckeditor',
    'ckeditor_uploader',
    'daterange_filter',
    'easy_thumbnails',
    'image_cropping',

    # --modules
    'cart',
    'search',
    'utils',
    # --apps
    'blog',
    'markdown_deux',
    'gallery',
    'catalog',
    'param',
    'filter',
    'order',
    'users',
    'home',
    'menu',
    'page',
    'site_info',
    'settings_template',
    'include_area'
]

# from easy_thumbnails.conf import Settings as thumbnail_settings
# THUMBNAIL_PROCESSORS = (
#     'image_cropping.thumbnail_processors.crop_corners',
# ) + thumbnail_settings.THUMBNAIL_PROCESSORS


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'htmlmin.middleware.HtmlMinifyMiddleware',
    'htmlmin.middleware.MarkRequestMiddleware',
]


ROOT_URLCONF = 'pyap.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, '..', 'theme', 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'libraries': {
                'get_include_area': 'include_area.templatetags.get_include_area',
            }
        },

    },
]


WSGI_APPLICATION = 'pyap.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'pyap_v200.sqlite3'),
    }
}
# # --local DB
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': 'pyap_v200',
#         'USER': 'pyap',
#         'PASSWORD': 'pyap_81086',
#         'HOST': 'localhost',
#         'PORT': '',
#     }
# }


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'


STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'pyap', 'static'),
    os.path.join(BASE_DIR, '..', 'app', 'static'),
)
STATIC_ROOT = os.path.join(BASE_DIR, '..', 'theme', 'static')
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)
COMPRESS_ENABLED = True
COMPRESS_PRECOMPILERS = (
    ('text/stylus', 'stylus -u nib < {infile} > {outfile}'),
)


MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, '..', 'theme', 'media')


FILEBROWSER_DIRECTORY = ''
DIRECTORY = ''
MAX_UPLOAD_SIZE = 60485760
FILEBROWSER_MAX_UPLOAD_SIZE = MAX_UPLOAD_SIZE
FILEBROWSER_VERSIONS = {
    'admin_thumbnail': {'verbose_name': 'Admin Thumbnail', 'width': 60, 'height': 60, 'opts': 'crop'},
    'small': {'verbose_name': 'Small (2 col)', 'width': 140, 'height': '', 'opts': ''},
    'medium': {'verbose_name': 'Medium (4col )', 'width': 300, 'height': '', 'opts': ''},
    'big': {'verbose_name': 'Big (6 col)', 'width': 460, 'height': '', 'opts': ''},
    'large': {'verbose_name': 'Large (8 col)', 'width': 680, 'height': '', 'opts': ''},
    'max': {'verbose_name': 'Max (12 col)', 'width': 1600, 'height': '', 'opts': ''},
}
FILEBROWSER_ADMIN_VERSIONS = ['thumbnail', 'small', 'medium', 'big', 'large', 'max']
FILEBROWSER_ADMIN_THUMBNAIL = 'admin_thumbnail'


# Easy Thumbnails

THUMBNAIL_ALIASES = {
    '': {
        'thumb': {'size': (60, 60), 'crop': True},
        'small': {'size': (150, 150), 'crop': True},
        'medium': {'size': (300, 300), 'crop': True},
        'big': {'size': (680, 680), 'crop': True},
        'large': {'size': (1600, 1600), 'crop': True}
    }
}


CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'full',
        'width': '100%',
    },
}
CKEDITOR_IMAGE_BACKEND = 'pillow'
CKEDITOR_BROWSE_SHOW_DIRS = True
CKEDITOR_UPLOAD_PATH = 'uploads/'
CKEDITOR_RESTRICT_BY_DATE = True


SITE_ID = 1

LOGIN_URL = '/users/login/'

CART_SESSION_ID = 'cart'

# APPEND_SLASH = False

# AUTH_USER_MODEL = 'users.models'

# TEMPLATE_DEBUG = False

EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_HOST_USER = 'pyapmail@ya.ru'
EMAIL_HOST_PASSWORD = 'pjkmlfuthlf'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
ADMIN_EMAIL = EMAIL_HOST_USER
# # --- DURUNG DEVELOPMENT ONLY ---
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# # ------------------------------------------------------------------
