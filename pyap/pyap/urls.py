from django.urls import path, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from filebrowser.sites import site as filebrowser_site
from django.contrib.auth.views import PasswordResetDoneView, PasswordResetView
from django.contrib.sitemaps.views import sitemap
from seo.init import sitemaps
from seo.views import robots_txt

urlpatterns = [
    # path('articles/<slug:title>/<int:section>/', views.section, name='article-section'),
    # path('weblog/', include('blog.urls')),

    path('admin/filebrowser/', filebrowser_site.urls),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls),
    path('', include('django.contrib.auth.urls')),
    path('admin/password_reset/', PasswordResetView.as_view(), name='admin_password_reset'),
    path('admin/password_reset/done/', PasswordResetDoneView.as_view, name='password_reset_done'),

    path('ckeditor/', include('ckeditor_uploader.urls')),

    path('', include('home.urls')),
    path('order/', include('order.urls')),
    path('users/', include('users.urls')),
    path('page/', include('page.urls')),
    # # url(r'review/', include('review.urls')),
    #
    path('blog/', include('blog.urls')),
    path('gallery/', include('gallery.urls')),
    #
    # # modules
    path('search/', include('search.urls')),
    path('cart/', include('cart.urls')),
    #
    path(r'sitemap\.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    path(r'robots\.txt', robots_txt, name='robots_txt'),
    # # ---- Приоритет имеет значение! ----
    path(r'catalog/', include('catalog.urls')),
    # path(r'', include('product.urls')),
    # path(r'dev_init/', include('dev_init.urls')),

    # path(r'(?P<url>[_\-\w\d]+)/$', url_hendler, name='url_handler'),
    # path(r'defauldb/(?P<app>[_\-\w\d]+)/$', AddDefaultDB.as_view()),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)\
                   + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

