from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField
from utils.thumbnail import resize
from generic.abstract.models import AbstractImageModel
from utils.help_text import SOCIAL_NETWORK_HTML_LINK_HT
from utils.url import generate_path_year_month


class SocialNetwork(AbstractImageModel):
    """
    Настрока социальных сетей приложения, сайта
    """

    class Meta:
        verbose_name = 'социальная сеть'
        verbose_name_plural = 'социальные сети'

    title = models.CharField(max_length=255, verbose_name='Заголовок', blank=True, null=True)
    image = ThumbnailerImageField(upload_to=generate_path_year_month, blank=True, null=True, verbose_name='Иконка')
    html_link = models.CharField(
        verbose_name='НТМЛ-ссылка', max_length=512, unique=True, blank=True, null=True,
        help_text=SOCIAL_NETWORK_HTML_LINK_HT)
    url = models.URLField(verbose_name='URL страницы', blank=True, null=True)

    def __str__(self):
        return self.title

    def get_html_links(self):
        """Получить все ссылки, заключенные в HTML код."""
        return SocialNetwork.objects.filter(image__isnull=True, html_link__isnull=False).values('html_link',)

    def save(self, *args, **kwargs):
        super(SocialNetwork, self).save(*args, **kwargs)
        # --resizing image
        if self.image and (self.image.width > 200 or self.image.height > 200):
            resize(self.image.path, (200, 200), True)

