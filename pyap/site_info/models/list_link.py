from django.db import models
from utils.help_text import SETTINGSTEMPLATE_TYPE_LINK_HT


class ListLink(models.Model):
    """
    Быстрые ссылки
    """
    LINK = 'L'
    TAG = 'T'
    TYPE_LINK_CHOICES = (
        (LINK, 'ссылки'),
        (TAG, 'тэги')
    )

    class Meta:
        verbose_name = 'быстрая ссылка'
        verbose_name_plural = 'быстрые ссылки'

    title = models.CharField(max_length=125, verbose_name='Заголовок', db_index=True)
    url = models.URLField(max_length=255, verbose_name='URL страницы', blank=True, null=True, db_index=True)
    sort = models.PositiveSmallIntegerField(verbose_name='Порядок отображения', db_index=True, default=0)
    is_show = models.BooleanField(default=True, verbose_name='Показать/Скрыть')
    type_link = models.CharField(
        max_length=1, choices=TYPE_LINK_CHOICES, default=TYPE_LINK_CHOICES[0], verbose_name='Вид ссылки',
        help_text=SETTINGSTEMPLATE_TYPE_LINK_HT)

    def __str__(self):
        return self.title
