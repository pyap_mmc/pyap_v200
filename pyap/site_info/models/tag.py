from django.db import models


class Tag(models.Model):
    """
    Тэги
    """
    class Meta:
        ordering = ('title',)
        verbose_name = 'Тэг'
        verbose_name_plural = 'Тэги'

    title = models.CharField(max_length=255, verbose_name='Название')

    def __str__(self):
        return self.title
