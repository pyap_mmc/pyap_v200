from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField


class TextInfo(models.Model):
    """
    Текстовая информация
    """

    class Meta:
        verbose_name = 'текстовая информация'
        verbose_name_plural = 'текстовые информации'

    title = models.CharField(max_length=125, verbose_name='Заголовок', unique=True)
    html = RichTextUploadingField(verbose_name='Текст/HTML')

    def __str__(self):
        return self.title

