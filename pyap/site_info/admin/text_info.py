from django.contrib import admin
from site_info.models import TextInfo
from generic.abstract.admin import AbstractDefaultAdmin


@admin.register(TextInfo)
class TextInfoAdmin(AbstractDefaultAdmin):
    search_fields = ('title',)
    list_display = ('title', 'get_html')
    list_filter = ('title',)


