from django.contrib import admin
from generic.abstract.admin import AbstractDefaultAdmin, AbstractImageAdmin
from site_info.models import SocialNetwork


@admin.register(SocialNetwork)
class SocialNetworkAdmin(AbstractDefaultAdmin, AbstractImageAdmin):
    search_fields = ('title', 'image_title')
    list_display = AbstractImageAdmin.list_display + ('title', 'html_link', 'url')

