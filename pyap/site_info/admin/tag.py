from django.contrib import admin
from site_info.models import Tag
from generic.abstract.admin import AbstractDefaultAdmin


@admin.register(Tag)
class TagAdmin(AbstractDefaultAdmin):
    pass


