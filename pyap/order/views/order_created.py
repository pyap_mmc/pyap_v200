from __future__ import unicode_literals
from django.views.generic.base import TemplateView
from generic.mixins import MainPageMixin


class OrderCreated(MainPageMixin, TemplateView):
    """
    Страница успешного оформления зкаказа и перехода к нему
    """
    template_name = 'order/templates/created.html'

    def get_context_data(self, **kwargs):
        context = super(OrderCreated, self).get_context_data(**kwargs)
        context['breadcrumbs'] = [
            {'title': 'Корзина', 'url': '/cart/'},
            {'title': 'Заказ {}'.format(context['pk']),
                'url': 'users/{}/order'.format(context['pk'])}
        ]
        return context


