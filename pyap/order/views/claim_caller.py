from django.shortcuts import get_object_or_404
from django.views.generic.base import TemplateView
from generic.mixins import MainPageMixin
from django.shortcuts import redirect
from django.contrib import messages
from ..models import Claim
from ..forms import ClaimCallerForm


class ClaimCallerView(MainPageMixin, TemplateView):
    form_class = ClaimCallerForm
    model = Claim
    # template_name = 'home/home.html'

    def post(self, request, *args, **kwargs):
        obj = Claim.objects.create(
            fio=request.POST.get('fio'),
            phone=request.POST.get('phone'),
            typeof=Claim.CALLER
        )
        form = ClaimCallerForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Заявка оформлена!')
        else:
            messages.error(request, '(: Произошла ошибка при отправке оформелнии...')
        return redirect('/')
