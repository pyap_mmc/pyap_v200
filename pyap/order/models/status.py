from __future__ import unicode_literals
from django.db import models


class Status(models.Model):
    """Модель Статусы заказов"""

    name = models.CharField(max_length=255, unique=True, verbose_name='Наименование')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Статус заказа'
        verbose_name_plural = 'Статусы заказов'
