from __future__ import unicode_literals
from decimal import Decimal
from django.core.validators import MinValueValidator
from django.db import models


class Story(models.Model):
    """
    Модель содержит Историю жизни заказа.
    *Сохраняет историю заказа о выполненых действиях.
    *Отслеживает изменение стоимости заказа на каждом этапе.
    """
    order = models.ForeignKey('order.Order', verbose_name='Заказ', on_delete=models.CASCADE)
    status = models.ForeignKey('order.Status', null=True, verbose_name='Статус заказа', on_delete=models.SET_NULL)
    total_cost = models.DecimalField(
        verbose_name='Общая стоимость', max_digits=10, decimal_places=2,
        default=0, validators=[MinValueValidator(Decimal('0'))])
    comment = models.TextField(verbose_name='Комментарий', null=True)
    created = models.DateTimeField(verbose_name='Создан', auto_now_add=True)

    def __str__(self):
        return str(self.order)

    @staticmethod
    def create_or_update(order, message=''):
        """
        Создание / Обновлние Истории жизни заказа.
        Создать новый, если `заказ, статус и общая цена` уникальны, иначе обновить данные
        """
        order.total_cost = order.get_total_cost()
        story, is_created = Story.objects.get_or_create(
            order_id=order.id, status=order.status, total_cost=order.total_cost,
            defaults={
                'comment': message,
                'status': order.status,
                'total_cost': order.total_cost
            })

        # обновление существующего пункта истории
        br = '\r' if message else ''
        if not is_created:
            story.comment += br + message
            story.status = order.status
            story.total_cost = order.total_cost
            story.save(update_fields=('comment', 'status', 'total_cost'))

    class Meta:
        unique_together = ('order', 'status', 'total_cost')
        verbose_name = 'Заказ:История'
        verbose_name_plural = 'Заказ:История'
