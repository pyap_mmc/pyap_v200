from decimal import Decimal
from django.core.validators import MinValueValidator
from django.db import models
from catalog.models import ProductItem
from .story import Story


MSG_PRODUCT_PRICE_CHANGE = '[*]Цена изменена. Товар:`{}` {} x {} = {} p'
MSG_PRODUCT_QTY_CHANGE = '[*]Кол-во изменено. Товар:`{}` {} x {} = {} p'
MSG_PRODUCT_ADD = '[+]Добавлен товар `{}` {} x {} = {} p'
MSG_PRODUCT_DELETE = '[-]Удален товар `{}` {} x {} = {} p'


class OrderItem(models.Model):
    class Meta:
        verbose_name = 'пункт заказа'
        verbose_name_plural = 'пункты заказа'

    order = models.ForeignKey('order.Order', verbose_name='Заказ', on_delete=models.CASCADE)
    product_item = models.ForeignKey(ProductItem, verbose_name='Вариант товара', on_delete=models.CASCADE)
    price = models.DecimalField(
        verbose_name='Цена', max_digits=10, decimal_places=2, validators=[MinValueValidator(Decimal('0'))])
    quantity = models.PositiveSmallIntegerField(verbose_name='Кол-во', default=1)

    def __str__(self):
        return '{}'.format(self.id)

    def get_cost(self):
        """Получить стоимость заказанной позиции"""
        return self.price * self.quantity

    def save(self, *args, **kwargs):
        """
        При изменении данных пункта в может измениться общая сумма заказа
        1. При добавление/ удалении товара
        2. При изменении кол-ва / цены товара
        """
        try:
            old_obj = OrderItem.objects.get(id=self.id)
        except OrderItem.DoesNotExist:
            old_obj = self

        old_qty = old_obj.quantity
        old_count_items = self.order.orderitem_set.count()

        super(OrderItem, self).save(*args, **kwargs)

        current_count_items = self.order.orderitem_set.count()
        current_qty = self.quantity

        # изменении цены товара
        if old_obj.price != self.price:
            message = MSG_PRODUCT_PRICE_CHANGE.format(
                self.product_item, self.price, self.quantity, self.get_cost())
            Story.create_or_update(self.order, message)

        # изменение кол-ва у варианта товара
        if current_qty != old_qty:
            message = MSG_PRODUCT_QTY_CHANGE.format(
                self.product_item, self.price, self.quantity, self.get_cost())
            Story.create_or_update(self.order, message)

        # добавление нового варианта товара
        if old_count_items < current_count_items:
            message = MSG_PRODUCT_ADD.format(
                self.product_item, self.price, self.quantity, self.get_cost())
            Story.create_or_update(self.order, message)

        # заново сохранить заказ, т.к. данные изменены
        self.order.save()

    def delete(self, *args, **kwargs):
        old_count_items = self.order.orderitem_set.count()
        super(OrderItem, self).delete(*args, **kwargs)
        current_count_items = self.order.orderitem_set.count()

        # удаление варианта товара
        if old_count_items > current_count_items:
            message = MSG_PRODUCT_DELETE.format(
                self.product_item, self.price, self.quantity, self.get_cost())
            Story.create_or_update(self.order, message)

        # заново сохранить заказ, т.к. данные изменены
        self.order.save()


