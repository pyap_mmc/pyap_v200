from django.db import models
from generic.abstract.models import AbstractCreatedModel
from django.contrib.auth.models import User


class Claim(AbstractCreatedModel):
    """
    Модель заявки.
    """
    class Meta:
        verbose_name = 'Заявка'
        verbose_name_plural = 'Заявки'
        ordering = ('-created',)

    REQUEST = 0
    CALLER = 1
    TYPEOF_CHOICES = (
        (REQUEST, 'Заявка'),
        (CALLER, 'Звонок')
    )

    fio = models.CharField(max_length=255, verbose_name='Ф.И.О')
    email = models.EmailField(verbose_name='Email')
    phone = models.CharField(max_length=100, verbose_name='Телефон', null=True, blank=True)
    user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.SET_NULL, null=True, blank=True)
    typeof = models.PositiveSmallIntegerField('Тип', choices=TYPEOF_CHOICES, default=CALLER)
    status = models.ForeignKey('order.Status', verbose_name='Статус', null=True, on_delete=models.SET_NULL)
    comment = models.TextField(verbose_name='Комментарий', blank=True, null=True)

    def __str__(self):
        return '{}: {}'.format(
            self.pk, self.get_typeof_display(),
        )



