from __future__ import unicode_literals
from decimal import Decimal
from django.core.validators import MinValueValidator
from django.db import models
from generic.abstract.models import AbstractCreatedModel
from django.contrib.auth.models import User
from .story import Story


class Order(AbstractCreatedModel):
    """
    Модель заказа.
    """
    user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50, verbose_name='Имя')
    last_name = models.CharField(max_length=50, verbose_name='Фамилия')
    email = models.EmailField(verbose_name='Email')
    address = models.CharField(verbose_name='Адрес', max_length=250)
    postal_code = models.CharField(max_length=20, verbose_name='Почтовый индекс', null=True, blank=True)
    city = models.CharField(max_length=100, verbose_name='Город', null=True, blank=True)
    total_cost = models.DecimalField(
        verbose_name='Общая стоимость', max_digits=10, decimal_places=2, validators=[MinValueValidator(Decimal('0'))],
        help_text=' Автоматический расчет при сохранении.')
    status = models.ForeignKey('order.Status', verbose_name='Статус заказа', null=True, on_delete=models.CASCADE)
    ttn = models.CharField('Товарно-транспортная накладная', blank=True, null=True, max_length=128)
    comment = models.TextField(verbose_name='Комментарий', blank=True, null=True)

    def __str__(self):
        return 'Заказ: {}'.format(self.id)

    def get_user_full_name(self):
        """Полное ФИО грузополучателя. Может отличаться от того, кто делает заказ"""
        full_name = '%s %s' % (self.last_name, self.first_name)
        if (self.first_name in self.user.first_name) and (self.last_name in self.user.last_name):
            full_name = self.user.get_full_name()
        return full_name

    def get_total_qty(self):
        """Общее кол-во заказанных товаров"""
        return sum(item.quantity for item in self.orderitem_set.all())

    def get_total_cost(self):
        """Получить общую стоимость(все варианты позиций в заказе)"""
        return sum(item.get_cost() for item in self.orderitem_set.all())

    def save(self, *args, **kwargs):
        # # Обрабатывается при втором и последующих обновлений заказа
        if not self.total_cost:
            self.total_cost = self.get_total_cost()
        if self.id:
            Story.create_or_update(order=self)

        super(Order, self).save(*args, *kwargs)

    class Meta:
        verbose_name = 'заказ'
        verbose_name_plural = 'заказы'
        ordering = ('-created', )


