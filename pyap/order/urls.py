from django.urls import path
from .views.order_create import OrderCreate
from .views.order_created import OrderCreated
from .views.claim_caller import ClaimCallerView

urlpatterns = [
    path(r'create/', OrderCreate.as_view(), name='order-create'),
    path(r'<int:pk>/created/', OrderCreated.as_view(), name='order-created'),
    path(r'claim-caller/create/', ClaimCallerView.as_view(), name='claim_caller'),
]
