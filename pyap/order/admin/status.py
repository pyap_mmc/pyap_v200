from __future__ import unicode_literals
from django.contrib import admin
from utils.abstract_admin import DefaultSettings
from order.models import Status


@admin.register(Status)
class StatusAdmin(DefaultSettings):
    menu_title = 'Заказы:Статусы'
    menu_group = 'Заказы'
    search_fields = ('name',)
