from __future__ import unicode_literals
from django.contrib import admin
from daterange_filter.filter import DateRangeFilter
from utils.abstract_admin import DefaultSettings
from order.models import Story


@admin.register(Story)
class StoryAdmin(DefaultSettings):
    menu_title = 'Заказы:История'
    menu_group = 'Заказы'
    date_hierarchy = 'created'
    search_fields = ('order_id', 'status')
    list_filter = (
        'status',
        ('created', DateRangeFilter), ('updated', DateRangeFilter),
    )
    list_display = ('order', 'status', 'total_cost', 'created', 'comment')
    raw_id_fields = ('order',)
