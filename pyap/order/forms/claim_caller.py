from django import forms
from django.forms import widgets

from generic.widgets import TextInputWidget
from order.models import Claim


class ClaimCallerForm(forms.ModelForm):
    class Meta:
        model = Claim
        fields = ('fio', 'phone', 'user', 'typeof')

    fio = forms.CharField(
        label='Ф.И.О',
        widget=TextInputWidget(attrs={'placeholder': 'Ф.И.О'})
    )
    phone = forms.CharField(
        label='Телефон',
        widget=TextInputWidget(attrs={'placeholder': 'Телефон'})
    )

    def __init__(self, *args, **kwargs):
        super(ClaimCallerForm, self).__init__(*args, **kwargs)

        self.fields['user'].widget = widgets.HiddenInput()
        self.fields['typeof'].widget = widgets.HiddenInput()

        if 'initial' in kwargs.keys():
            request = kwargs['initial']['request']
            if request.user.is_authenticated:
                self.fields['user'].initial = request.user
                self.fields['typeof'].initial = Claim.CALLER
                # self.fields['fio'].initial = request.user.fio
                # self.fields['phone'].initial = request.user.phone