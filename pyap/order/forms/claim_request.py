from django import forms
from django.forms import widgets

from generic.widgets import TextInputWidget
from order.models import Claim


class ClaimRequestForm(forms.ModelForm):
    class Meta:
        model = Claim
        fields = ('fio', 'email', 'phone', 'comment', 'user', 'typeof')

    def __init__(self, *args, **kwargs):
        super(ClaimRequestForm, self).__init__(*args, **kwargs)

        self.fields['user'].widget = widgets.HiddenInput()
        self.fields['typeof'].widget = widgets.HiddenInput()

        if 'initial' in kwargs.keys():
            request = kwargs['initial']['request']
            if request.user.is_authenticated:
                self.fields['user'].initial = request.user
                self.fields['typeof'].initial = Claim.REQUEST
                # self.fields['email'].initial = request.user.email
                # self.fields['fio'].initial = request.user.fio
                # self.fields['phone'].initial = request.user.phone
