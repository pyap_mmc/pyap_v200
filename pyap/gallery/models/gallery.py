from django.urls import reverse
from generic.abstract.models import AbstractMPTTPageSeoModel
from .gallery_image import GalleryImage


class Gallery(AbstractMPTTPageSeoModel):
    """
    Модель Галерея. Поддерживает вложенность.
    редназначена для привязки к одному объекту-разделу множество фотографий.
    Применение: подходит для создания различного рода вывода массого списка изображений.
    """
    class Meta:
        verbose_name = 'Галерею'
        verbose_name_plural = 'Галереи'
        unique_together = ('title', 'parent', 'slug')

    class MPTTMeta:
        order_insertion_by = ('parent', 'sort')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug,
            # 'year': '%04d' % self.created.year,
            # 'month': '%02d' % self.created.month,
            # 'day': '%02d' % self.created.day,
        }
        return reverse('gallery', kwargs=kwargs)

    def get_images(self):
        return GalleryImage.objects.filter(gallery_id=self.id)


