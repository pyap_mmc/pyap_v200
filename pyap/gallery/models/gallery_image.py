from django.db import models
from generic.abstract.models import AbstractImageModel


class GalleryImage(AbstractImageModel):
    """
    Фотографии для раздела Галереи
    """
    gallery = models.ForeignKey('gallery.Gallery', verbose_name='Галерея', on_delete=models.CASCADE)

    def __str__(self):
        return self.gallery.title

    def save(self, *args, **kwargs):
        self.set_image_title(self.gallery)
        super(GalleryImage, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Фотографию'
        verbose_name_plural = 'Фотографии'

