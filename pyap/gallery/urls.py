from django.urls import path
from .views.gallery_detail import GalleryDetailView


urlpatterns = [
    path(r'<str:slug>/', GalleryDetailView.as_view(), name='gallery'),
]
