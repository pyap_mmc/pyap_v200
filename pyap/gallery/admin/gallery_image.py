from django.contrib import admin
from generic.abstract.admin import AbstractDefaultAdmin, AbstractImageAdmin
from ..models.gallery_image import GalleryImage


@admin.register(GalleryImage)
class GalleryImageAdmin(AbstractDefaultAdmin, AbstractImageAdmin):
    raw_id_fields = ('gallery',)
    list_filter = ('gallery',) + AbstractImageAdmin.list_filter
    list_display = ('gallery',) + AbstractImageAdmin.list_display