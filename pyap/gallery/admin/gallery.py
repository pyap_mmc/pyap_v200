from django.contrib import admin
from generic.abstract.admin import AbstractMPTTPageSeoAdmin, AbstractImageInlineAdmin, fields_element
from ..models.gallery import Gallery
from ..models.gallery_image import GalleryImage


class GalleryImageInline(AbstractImageInlineAdmin):
    model = GalleryImage


@admin.register(Gallery)
class GalleryAdmin(AbstractMPTTPageSeoAdmin):
    inlines = (GalleryImageInline,)

    fieldsets = (
        fields_element(('parent',)),
        AbstractMPTTPageSeoAdmin.fieldsets[0],
        AbstractMPTTPageSeoAdmin.fieldsets[2],
        AbstractMPTTPageSeoAdmin.fieldsets[3],
    )