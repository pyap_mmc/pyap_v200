from django.views.generic.base import TemplateView
from generic.mixins import MainPageMixin
from utils.pagination import get_pagination
from utils.leftbar import get_leftbar
from utils.sort import sort_by_params
from ..models.post import Post
from ..models.blog import Blog


class BlogListView(MainPageMixin, TemplateView):
    """
    Блог и список его постов
    """
    template_name = 'blog/templates/post_list.html'

    def get_context_data(self, **kwargs):
        context = super(BlogListView, self).get_context_data(**kwargs)
        context['object'] = Blog.objects.get(slug=context['slug'], is_show=True)
        context['leftbar'] = get_leftbar(Blog, context['object'])
        blog_id = context['leftbar']['root_obj'].id
        context['current_mainmenu'] = context['mainmenu'].filter(
            blog_id=blog_id, is_show=True,
        ).first()
        context['objects'] = Post.objects.filter(blog_id=context['object'].id, is_show=True)
        context['objects'] = sort_by_params(self.request, context['objects'])
        context['objects'] = get_pagination(self.request, context['objects'])
        return context

