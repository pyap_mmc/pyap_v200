from django.contrib import admin
from daterange_filter.filter import DateRangeFilter
from utils.abstract_admin import DefaultSettings
from ..models import Comment


@admin.register(Comment)
class CommentAdmin(DefaultSettings):
    raw_id_fields = ('post', 'user')
    date_hierarchy = 'created'
    search_fields = ('username', 'post__title', 'text')
    list_display = ('email', 'post', 'text', 'user', 'is_show')
    list_display_links = ('email', 'post')
    list_editable = ('is_show',)
    list_filter = (
        'is_show', 'created', 'updated'
        #('created', DateRangeFilter), ('updated', DateRangeFilter)
    )
