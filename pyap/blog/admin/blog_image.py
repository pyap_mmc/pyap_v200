from django.contrib import admin
from generic.abstract.admin import AbstractDefaultAdmin, AbstractImageAdmin
from ..models import BlogImage


@admin.register(BlogImage)
class BlockImageAdmin(AbstractDefaultAdmin, AbstractImageAdmin):
    raw_id_fields = ('blog',)
    list_filter = ('blog',) + AbstractImageAdmin.list_filter
    list_display = ('blog',) + AbstractImageAdmin.list_display


