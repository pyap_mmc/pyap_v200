from django.contrib import admin
from generic.abstract.admin import AbstractMPTTPageSeoAdmin, AbstractImageInlineAdmin, fields_element
from ..models import Blog, BlogImage


class BlogImageInline(AbstractImageInlineAdmin):
    model = BlogImage


@admin.register(Blog)
class BlogAdmin(AbstractMPTTPageSeoAdmin):
    inlines = (BlogImageInline,)

    fieldsets = (
        fields_element(('parent',)),
        AbstractMPTTPageSeoAdmin.fieldsets[0],
        AbstractMPTTPageSeoAdmin.fieldsets[2],
        AbstractMPTTPageSeoAdmin.fieldsets[3],
    )
