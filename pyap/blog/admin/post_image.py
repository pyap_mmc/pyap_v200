from django.contrib import admin
from generic.abstract.admin import AbstractDefaultAdmin, AbstractImageAdmin
from ..models import PostImage


@admin.register(PostImage)
class PostImageAdmin(AbstractDefaultAdmin, AbstractImageAdmin):
    raw_id_fields = ('post',)
    list_filter = ('post',) + AbstractImageAdmin.list_filter
    list_display = ('post',) + AbstractImageAdmin.list_display


