from django.urls import path

from .views.blog_list import BlogListView
from .views.post_view import PostView


urlpatterns = [
    path(r'<str:slug>/', BlogListView.as_view(), name='blog_index'),
    path(r'<str:blog_slug>/<str:post_slug>/', PostView.as_view(), name='blog_detail'),
]
