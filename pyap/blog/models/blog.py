from django.urls import reverse
from generic.abstract.models import AbstractMPTTPageSeoModel
from .blog_image import BlogImage


class Blog(AbstractMPTTPageSeoModel):
    """Блок. Включает в себю множество посты :model:`blog.Post`."""

    class Meta:
        verbose_name = 'Блог'
        verbose_name_plural = 'Блоги'

    def get_absolute_url(self):
        kwargs = {'slug': self.slug}
        return reverse('blog_index', kwargs=kwargs)

    def get_images(self):
        return BlogImage.objects.filter(blog_id=self.id)

