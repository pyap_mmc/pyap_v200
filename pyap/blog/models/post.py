from django.urls import reverse
from django.db import models
from generic.abstract.models import AbstractPageSeoModel
from .post_image import PostImage
from .post_comment import Comment


class Post(AbstractPageSeoModel):
    """
    Посты для блога
    """
    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'
        ordering = ('blog', 'sort', 'is_show')

    blog = models.ForeignKey('blog.Blog', verbose_name='Блог', blank=True, null=True, on_delete=models.SET_NULL)
    comment_count = models.IntegerField(blank=True, default=0, verbose_name='Кол-во коментариев')

    def get_images(self):
        """Вернуть все фотографии объекта"""
        return PostImage.objects.filter(post_id=self.id)

    def get_comments(self):
        qs = Comment.objects.filter(post_id=self.id)
        return qs

    def get_absolute_url(self):
        blog_slug = self.blog.slug if self.blog and self.blog.slug else f'blog{self.id}'
        post_slug = self.slug if self.slug else f'blog{self.slug}'
        kwargs = {
            'blog_slug': blog_slug,
            'post_slug': post_slug
        }
        return reverse('blog_detail', kwargs=kwargs)


