from django.db import models
from generic.abstract.models import AbstractImageModel


class BlogImage(AbstractImageModel):
    """
    Фотографии для раздела Блога :model:`blog.Blog`.
    """
    blog = models.ForeignKey('blog.Blog', verbose_name='Блог', on_delete=models.CASCADE)

    def __str__(self):
        return self.blog.title

    def save(self, *args, **kwargs):
        self.set_image_title(self.blog)
        super(BlogImage, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Блог:Фото'
        verbose_name_plural = 'Блог:Фото'

