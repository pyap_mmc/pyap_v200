from django.db import models
from django.db.models.signals import post_save
from generic.abstract.models import AbstractCommentModel
from ..signals import save_comment


class Comment(AbstractCommentModel):
    """
    Коментарии к посту
    """
    post = models.ForeignKey('blog.Post', verbose_name='Пост', null=True, on_delete=models.SET_NULL)


post_save.connect(save_comment, sender=Comment)
