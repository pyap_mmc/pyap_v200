from django.db import models
from generic.abstract.models import AbstractImageModel


class PostImage(AbstractImageModel):
    """
    Фотографии для Поста :model:`blog.Post`.
    """
    post = models.ForeignKey('blog.Post', verbose_name='Статья/Пост', on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.set_image_title(self.post)
        super(PostImage, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Посты:Фото'
        verbose_name_plural = 'Посты:Фото'
