# Generated by Django 2.1 on 2018-08-09 19:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_auto_20180809_1842'),
    ]

    operations = [
        migrations.AddField(
            model_name='blog',
            name='is_allow_comments',
            field=models.BooleanField(default=False, verbose_name='разрешить комментарии'),
        ),
    ]
