from django.contrib import admin
from mptt.admin import TreeRelatedFieldListFilter
from settings_template.models import Footer
from generic.abstract.admin import AbstractDefaultAdmin


@admin.register(Footer)
class FooterAdmin(AbstractDefaultAdmin):
    radio_fields = {'text_info': admin.VERTICAL}
    filter_horizontal = ('list_link', 'catalog', 'page', 'blogs', 'galleries')
    search_fields = ('title',)
    list_display = ('title', 'is_show', 'text_info')
    list_editable = ('is_show',)
    list_filter = (
        'is_show', 'list_link', 'text_info', 'page', 'catalog',
    )

    fieldsets = (
        ('Основные данные и настройки сайта', {
            'classes': ('suit-tab', 'suit-tab-data'),
            'fields': ('title', 'is_show',),
        }),
        ('Подключение элементов к шаблону', {
            'fields': ('catalog', 'page', 'galleries', 'blogs'),
            'classes': ('suit-tab', 'suit-tab-include'),
        }),
        ('Дополнительные элементы для футера', {
            'fields': ('text_info', 'list_link'),
            'classes': ('suit-tab', 'suit-tab-more'),
        }),
    )

    suit_form_tabs = (
        ('data', 'ДАННЫЕ САЙТА'),
        ('include', 'ПОДКЛЮЧЕНИЕ'),
        ('more', 'ДОПОЛНИТЕЛЬНО'),
    )
