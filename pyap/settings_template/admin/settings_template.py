from django.contrib import admin
from settings_template.models.settings_template import SettingsTemplate
from generic.abstract.admin import AbstractDefaultAdmin, AbstractImageAdmin


@admin.register(SettingsTemplate)
class SettingsTemplateAdmin(AbstractDefaultAdmin, AbstractImageAdmin):
    search_fields = ('name', 'parent')
    actions = AbstractDefaultAdmin.actions + AbstractImageAdmin.actions
    list_filter = ('is_included', 'home', 'footer')
    list_display = ('title', 'thumb', 'email', 'phone', 'address', 'is_included')
    list_display_links = ('title', 'thumb',)
    list_editable = ('is_included',)

    fieldsets = (
        ('Основные данные и настройки сайта', {
            'classes': ('suit-tab', 'suit-tab-data'),
            'fields': ('title', 'is_included', 'site', 'email', 'phone', 'address', 'logo'),
        }),
        ('Подключение элементов к шаблону', {
            'fields': ('home', 'footer'),
            'classes': ('suit-tab', 'suit-tab-include'),
        }),
        ('Сео и МЕТА настройки', {
            'fields': ('robots_txt', 'meta'),
            'classes': ('suit-tab', 'suit-tab-meta'),
        }),
        ('Скрипты JavaScripts', {
            'fields': ('scripts',),
            'classes': ('suit-tab', 'suit-tab-scripts'),
        }),
        ('Прочие данные', {
            'fields': ('terms_of_use',),
            'classes': ('suit-tab', 'suit-tab-terms'),
        }),
    )

    suit_form_tabs = (
        ('data', 'ДАННЫЕ САЙТА'),
        ('include', 'ПОДКЛЮЧЕНИЕ'),
        ('meta', 'CEO/META'),
        ('scripts', 'СКРИПТЫ'),
        ('terms', 'ПРОЧЕЕ'),
    )
