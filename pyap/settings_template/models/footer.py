from django.db import models
from catalog.models import Catalog
from page.models.page import Page
from blog.models.blog import Blog
from gallery.models.gallery import Gallery

from site_info.models import ListLink, TextInfo


class Footer(models.Model):
    """
    футера. Блок текста/блок ссылок(тегов).
    """
    title = models.CharField(
        max_length=125, verbose_name='Заголовок', unique=True, default='Нижний футер')
    list_link = models.ManyToManyField(ListLink, verbose_name='Быстрые ссылки', blank=True)
    text_info = models.ForeignKey(TextInfo, verbose_name='Текстовая информация', blank=True, null=True, on_delete=models.CASCADE)
    catalog = models.ManyToManyField(
        Catalog, verbose_name='Каталоги', limit_choices_to={'is_show': True}, blank=True)
    page = models.ManyToManyField(
        Page, verbose_name='Страницы', limit_choices_to={'is_show': True}, blank=True)
    blogs = models.ManyToManyField(
        Blog, verbose_name='Блоги', limit_choices_to={'is_show': True}, blank=True)
    galleries = models.ManyToManyField(
        Gallery, verbose_name='Галереи', limit_choices_to={'is_show': True}, blank=True)
    is_show = models.BooleanField(default=True, verbose_name='Отображать')

    def __str__(self):
        return self.title

    def get_list_link(self):
        return self.list_link.filter(is_show=True)

    class Meta:
        verbose_name = 'футер'
        verbose_name_plural = 'футер'
