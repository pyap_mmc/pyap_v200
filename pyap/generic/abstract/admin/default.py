import os
import sys
from django.contrib import admin, messages
from django.utils.html import mark_safe
from django.core.management import call_command, CommandError
from mptt.admin import MPTTModelAdmin, TreeRelatedFieldListFilter


# class AbstractDefaultAdmin(admin.ModelAdmin):
#     """
#     Стандартные настройки для всех классов
#     """
#     class Meta:
#         abstract = True
#
#     empty_value_display = '- нет -'
#     save_on_top = True
#     actions = ('clone_object',)
#
#     def get_html(self, obj):
#         """Вернуть ХТМЛ из строки `<object>.html`"""
#         return mark_safe('{}'.format(obj.html))  # [:300])
#     get_html.short_description = 'HTML'
#
#     def clone_object(self, request, queryset):
#         """Копирование(клонирование) выбранных объектов - action"""
#         for obj in queryset:
#             prefix = '-_CLONE'
#             clone = obj
#             clone.title += prefix
#             clone.slug += prefix
#             clone.seo_title = clone.seo_description = clone.seo_keywords = ''
#             clone.id = None
#             clone.is_show = False
#             clone.save()
#             messages.success(request, mark_safe('Склонирован <b>{}</b>', obj))
#     clone_object.short_description = 'Клонировать'


# class AbstractDefaultMPTTAdmin(MPTTModelAdmin, AbstractDefaultAdmin):
#     """Стандартные настройки для MPTT моделей"""
#
#     class Meta:
#         abstract = True
#
#     mptt_level_indent = 23
#     actions = ('rebuild',) + AbstractDefaultAdmin.actions
#
#     def rebuild(self, request, queryset):
#         """Пересорбать МПТТ модель. Иногда требуется для перезагрузки дерева."""
#         self.model.objects.rebuild()
#     rebuild.short_description = 'Пересобрать пункты раздела'


# class AbstractDefaultStackedInlineAdmin(admin.StackedInline):
#     extra = 0
#     show_change_link = True


# =============================================================================================================

def dir_name_object(obj):
    f = sys.modules[obj.__module__].__file__
    return os.path.splitext(os.path.basename(f))[0]


class BaseAdmin:
    empty_value_display = '- нет -'
    save_on_top = True

    def get_html(self, obj):
        """Вернуть ХТМЛ из строки `<object>.html`"""
        return mark_safe('{}'.format(obj.html))  # [:300])
    get_html.short_description = 'HTML'

    def clone_object(self, request, queryset):
        """Копирование(клонирование) выбранных объектов - action"""
        for obj in queryset:
            prefix = '-_CLONE'
            clone = obj
            clone.title += prefix
            clone.slug += prefix
            clone.seo_title = clone.seo_description = clone.seo_keywords = ''
            clone.id = None
            clone.is_show = False
            clone.save()
            messages.success(request, mark_safe('Склонирован <b>{}</b>', obj))
    clone_object.short_description = 'Клонировать'

    def set_fixtures(self, request, queryset, dir_name=None, filename='default.json'):
        if request.user.is_superuser:
            sysout = sys.stdout

            if dir_name is None:
                dir_name = dir_name_object(self)

            path = '{}/fixtures/{}'.format(dir_name, filename)
            sys.stdout = open(path, 'w')
            call_command('dumpdata', dir_name)
            sys.stdout = sysout
    set_fixtures.short_description = 'Фикстуры:Сохранить'

    def load_fixtures(self, request, queryset, dir_name=None, filename='default.json'):
        if request.user.is_superuser:
            if dir_name is None:
                dir_name = dir_name_object(self)
            try:
                path = '{}/fixtures/{}'.format(dir_name, filename)
                call_command('loaddata', path, verbosity=0)
            except CommandError:
                messages.error(request, "Ошибка выполнения команды:(")
    load_fixtures.short_description = 'Фикстуры:Загрузить'


class AbstractDefaultAdmin(admin.ModelAdmin, BaseAdmin):
    """
    Стандартные настройки для всех классов
    """
    class Meta:
        abstract = True

    empty_value_display = '- нет -'
    save_on_top = True
    actions = ('clone_object',)


# -------------------
# - DEFAULT MPTT -
# -------------------

class AbstractDefaultMPTTAdmin(MPTTModelAdmin, BaseAdmin):
    """
    Стандартные настройки для MPTT моделей
    """

    class Meta:
        abstract = True

    mptt_level_indent = 23

    search_fields = ('title', 'parent__title')
    actions = ('rebuild', 'clone_object')
    list_filter = ('parent',)

    def rebuild(self, request, queryset):
        """Пересорбать МПТТ модель. Иногда требуется для перезагрузки дерева."""
        self.model.objects.rebuild()
    rebuild.short_description = 'Пересобрать пункты раздела'


# -------------------
# - DEFAULT INLINE -
# -------------------

class BaseInlineAdmin:
    extra = 0
    show_change_link = True


class AbstractDefaultStackedInlineAdmin(BaseInlineAdmin, admin.StackedInline):
    pass


class AbstractDefaultTabularInlineAdmin(BaseInlineAdmin, admin.TabularInline):
    pass
