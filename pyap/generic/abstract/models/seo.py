from django.db import models
from utils.translit_field import translaton_field
from utils.help_text import SLUG_HT, SEO_TITLE_HT, SEO_KEYWORDS_HT, SEO_DESCRIPTION_HT, OG_LOCALE_HT, \
    SWTTINGSTEMPLATE_SCRIPTS_HT


class AbstractSeoModel(models.Model):
    """
    Сео настройки для объекта модели
    """

    class Meta:
        abstract = True

    slug = models.SlugField(verbose_name='элемент URL', db_index=True, help_text=SLUG_HT)
    seo_title = models.CharField(
        max_length=255, verbose_name='Seo title', blank=True, null=True, help_text=SEO_TITLE_HT)
    seo_description = models.TextField(
        max_length=510, verbose_name='Seo description', blank=True, null=True, help_text=SEO_DESCRIPTION_HT)
    seo_keywords = models.TextField(
        max_length=510, verbose_name='Seo keywords', blank=True, null=True, help_text=SEO_KEYWORDS_HT)
    og_locale = models.TextField(
        max_length=510, null=True, blank=True, default='ru_RU', verbose_name='og locale', help_text=OG_LOCALE_HT)
    scripts = models.TextField(
        null=True, blank=True, verbose_name="Блок скриптов", help_text=SWTTINGSTEMPLATE_SCRIPTS_HT)

    def save(self, *args, **kwargs):
        from utils.auto_seo import AutoSeo
        AutoSeo(page=self, title=self.title).default()
        # обрезать `slug` если симоволов больше 255
        if len(self.slug) > 255:
            short_title = '[!] {}'.format(self.title[:251])
            self.slug = translaton_field(short_title)
        super(AbstractSeoModel, self).save(*args, **kwargs)
