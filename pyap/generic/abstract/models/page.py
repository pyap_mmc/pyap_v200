from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from .created import AbstractCreatedModel
from .content import AbstractContentModel
from .seo import AbstractSeoModel


class AbstractPageSeoModel(AbstractContentModel, AbstractSeoModel, AbstractCreatedModel):
    class Meta:
        abstract = True

    def get_images(self):
        return None

    def get_main_image(self):
        images = self.get_images()
        if images:
            image = images.first()
            if images.filter(image_is_main=True).exists():
                image = images.filter(image_is_main=True).first()
            return image.image

    def set_image_title(self, obj=None):
        """
        Установить название фото. Если пусто - установить переданный `title` объекта
        Если объект не передан объекто считается `self`
        Установить главное изображение, если таого нет
        :param obj: {object} - QuerySet object
        """
        obj = obj if obj else self
        if hasattr(obj, 'title'):
            if not self.image_title and obj.title:
                self.image_title = obj.title
        if hasattr(obj, 'get_images'):
            if not obj.get_images().filter(image_is_main=True).exists():
                self.image_is_main = True


class AbstractMPTTPageSeoModel(MPTTModel, AbstractPageSeoModel):
    class MPTTMeta:
        order_insertion_by = ('parent', 'sort')

    class Meta:
        unique_together = ('title', 'parent', 'slug')
        abstract = True

    parent = TreeForeignKey(
        'self', null=True, related_name='subitems', blank=True, db_index=True,
        verbose_name='Родительский объект', on_delete=models.SET_NULL)
