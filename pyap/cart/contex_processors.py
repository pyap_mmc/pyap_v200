from cart.cart import Cart


def cart(request):
    """
    Общие данные для корзины (для всех страниц)
    """
    return {'cart': Cart(request)}

