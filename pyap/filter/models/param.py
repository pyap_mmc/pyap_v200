from django.db import models


class BaseParam(models.Model):

    class Meta:
        verbose_name = 'Параметр'
        verbose_name_plural = 'Параметры'
        abstract = True

    key = models.CharField(verbose_name='Ключ', unique=True, max_length=255)

    def __str__(self):
        return self.key


class StrParam(BaseParam):

    class Meta:
        verbose_name = 'Строковый параметр'
        verbose_name_plural = 'Строковые параметры'


class IntParam(BaseParam):

    class Meta:
        verbose_name = 'Целочисленный параметр'
        verbose_name_plural = 'Целочисленные параметры'


class BoolParam(BaseParam):

    class Meta:
        verbose_name = 'Булев параметр'
        verbose_name_plural = 'Булевы параметры'

