from django.db import models
from ..models import StrParam, IntParam, BoolParam


class AbstractValueParam(models.Model):
    class Meta:
        verbose_name = 'Значение'
        verbose_name_plural = 'Значения'
        unique_together = ('param', 'value')
        abstract = True

    postfix = models.CharField(verbose_name='Постфикс', blank=True, null=True, max_length=125)

    def __str__(self):
        if self.postfix:
            return f'{self.param.key}: {self.value} {self.postfix}'
        return f'{self.param.key}: {self.value}'


class StrValueParam(AbstractValueParam):
    param = models.ForeignKey(StrParam, verbose_name='Параметр(стр.)', on_delete=models.CASCADE)
    value = models.CharField(verbose_name='Строковое значение', db_index=True, default='-', max_length=125)


class IntValueParam(AbstractValueParam):
    param = models.ForeignKey(IntParam, verbose_name='Параметр(число)', on_delete=models.CASCADE)
    value = models.IntegerField(verbose_name='Целочисленное значение', db_index=True, default=0)


class BoolValueParam(AbstractValueParam):
    param = models.ForeignKey(BoolParam, verbose_name='Параметр(булево)', on_delete=models.CASCADE)
    value = models.BooleanField(verbose_name='Булево значение', db_index=True, default=True)

    def __str__(self):
        value = 'Да' if self.value else 'Нет'
        if self.postfix:
            return f'{self.param.key}: {value} {self.postfix}'
        return f'{self.param.key}: {value}'
