# Generated by Django 2.1 on 2018-08-29 02:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('filter', '0002_auto_20180829_0252'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='strparam',
            name='value',
        ),
    ]
