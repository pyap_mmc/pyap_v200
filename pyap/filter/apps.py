from django.apps import AppConfig


class FilterConfig(AppConfig):
    name = 'filter'
    verbose_name = 'Фильтры'

