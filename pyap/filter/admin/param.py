from django.contrib import admin
from ..models import (StrParam, StrValueParam,
                      IntParam, IntValueParam,
                      BoolParam, BoolValueParam)


class AbstractValueInlineAdmin(admin.TabularInline):
    extra = 0


class StrValueInlineAdmin(AbstractValueInlineAdmin):
    model = StrValueParam


@admin.register(StrParam)
class StrParamAdmin(admin.ModelAdmin):
    inlines = (StrValueInlineAdmin,)


class IntValueInlineAdmin(AbstractValueInlineAdmin):
    model = IntValueParam


@admin.register(IntParam)
class IntParamAdmin(admin.ModelAdmin):
    inlines = (IntValueInlineAdmin,)


class BoolValueInlineAdmin(AbstractValueInlineAdmin):
    model = BoolValueParam


@admin.register(BoolParam)
class BoolParamAdmin(admin.ModelAdmin):
    inlines = (BoolValueInlineAdmin,)

