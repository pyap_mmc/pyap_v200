from django.contrib import admin
from ..models import StrValueParam


@admin.register(StrValueParam)
class StrValueParamAdmin(admin.ModelAdmin):
    pass
