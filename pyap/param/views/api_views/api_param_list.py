from rest_framework import generics, viewsets
from param.models import Param
from param.serializers import ParamAllListSerializer


class ParamAllListAPIView(generics.ListAPIView):
    """
    get:
    Получить список всех параметров.
    """

    queryset = Param.objects.all()
    serializer_class = ParamAllListSerializer



