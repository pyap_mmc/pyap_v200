from django.apps import AppConfig


class ParamConfig(AppConfig):
    name = 'param'
    verbose_name = 'Параметры'

