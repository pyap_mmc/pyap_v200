from django.db import models
from ..models import Param


class ParamSet(models.Model):
    class Meta:
        verbose_name = 'Набор параметров'
        verbose_name_plural = 'Наборы параметров'
        ordering = ('typeof',)

    PROMO_CREATE_AUTO = 0
    PROMO_HOME_FILTER = 1
    TYPEOF_CHOICES = (
        (PROMO_CREATE_AUTO, 'Новое объявление: автомобиль'),
        (PROMO_HOME_FILTER, 'Фильтр для главной страницы: объявления'),
    )

    typeof = models.PositiveSmallIntegerField(verbose_name='Тип', choices=TYPEOF_CHOICES, unique=True)
    params = models.ManyToManyField(Param, verbose_name='Параметры', blank=True)

    def __str__(self):
        return self.get_typeof_display()

    @staticmethod
    def promo_create_auto():
        return ParamSet.objects.get(typeof=ParamSet.PROMO_CREATE_AUTO).params.all().order_by('id')

    @staticmethod
    def promo_home_filter():
        return ParamSet.objects.get(typeof=ParamSet.PROMO_HOME_FILTER).params.all().order_by('id')