from django.db import models


class Param(models.Model):
    class Meta:
        verbose_name = 'Параметр'
        verbose_name_plural = 'Параметры'
        ordering = ('typeof',)

    STR = 'string'
    INT = 'integer'
    BOOL = 'bool'
    DECIMAL = 'decimal'
    TYPEOF_CHOICES = (
        (STR, 'Строковое значение'),
        (INT, 'Числовое(целое) значение'),
        (BOOL, 'Булево значение'),
        (DECIMAL, 'Денеждный формат'),
    )

    DECOR_COLOR = 'color'
    DECOR_CHOICES = (
        (DECOR_COLOR, 'Цвет'),
    )

    title = models.CharField(verbose_name='Название', max_length=255, unique=True)
    typeof = models.CharField(verbose_name='Тип параметра', default=STR, choices=TYPEOF_CHOICES, max_length=8)
    decor = models.CharField(
        verbose_name='Оформление, тип отоброжения', choices=DECOR_CHOICES, max_length=8,
        blank=True, null=True)

    def __str__(self):
        return self.title

    def promo_auto_create_values(self):
        """
        Параметры для создания объявления c Авто.
        :return qs: ParmValue for self
        """
        # temporal function!
        from .value import ParamValue
        return ParamValue.objects.filter(param=self.pk)

    def values(self):
        """
        Параметр и его значения.
        :return qs: ParmValue for self
        """
        from .value import ParamValue
        return ParamValue.objects.filter(param=self.pk)
