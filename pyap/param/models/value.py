from param.mixin.value import AbstractParamValueModel


class ParamValue(AbstractParamValueModel):
    class Meta:
        verbose_name = 'Значения параметра'
        verbose_name_plural = 'Значение параметра'


