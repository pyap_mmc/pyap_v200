from django import forms
from django.forms import widgets
from generic.forms import (
    CSS_CLASS_MODEL_CHOICES, CSS_CLASS_TEXT_INPUT
)


class ParamFieldMixin(forms.Form):
    """
    Миксин, инициализация параметров для форм
    """

    def init_one_bool_field(self, param, required=True):
        self.fields['param_' + str(param.pk)] = forms.BooleanField(
            required=required,
            label=param.title,
            widget=widgets.CheckboxInput(),
        )

    def init_one_int_field(self, param, required=True):
        self.fields['param_' + str(param.pk)] = forms.IntegerField(
            required=required,
            label=param.title,
            widget=widgets.NumberInput(attrs={
                'class': CSS_CLASS_TEXT_INPUT,
                'placeholder': param.title,
            }),
        )

    def init_one_decimal_field(self, param, required=True):
        self.fields['param_' + str(param.pk)] = forms.DecimalField(
            required=required,
            label=param.title,
            widget=widgets.NumberInput(attrs={
                'class': CSS_CLASS_TEXT_INPUT,
                'placeholder': param.title
            }),
        )

    def init_one_str_field(self, param, required=True):
        """
        инициализация/шаблон для параметра. Тип Строка, не имеющиймного значений:ParamValue
        """
        self.fields['param_' + str(param.pk)] = forms.CharField(
            required=required,
            label=param.title,
            widget=widgets.TextInput(attrs={
                'class': CSS_CLASS_TEXT_INPUT,
                'placeholder': param.title
            }),
        )

    def init_selected_field(self, param, required=True):
        """
        инициализация/шаблон для параметра, имеющий много значений.ParamValue Qs
        использовать в html: <select>
        """
        self.fields['param_' + str(param.pk)] = forms.ModelChoiceField(
            required=required,
            empty_label=':'+param.title,
            label=param.title,
            queryset=param.values(),
            widget=widgets.Select(attrs={'class': CSS_CLASS_MODEL_CHOICES})
        )
