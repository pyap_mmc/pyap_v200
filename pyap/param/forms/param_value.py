from django.forms import ModelForm
from ..models import ParamValue


class CreatePromoParamValueForm(ModelForm):

    class Meta:
        model = ParamValue
        fields = '__all__'
