from django.contrib import admin
from generic.abstract.admin.default import AbstractDefaultAdmin
from param.models import ParamSet


@admin.register(ParamSet)
class ParamSetAdmin(AbstractDefaultAdmin):
    filter_horizontal = ('params',)


