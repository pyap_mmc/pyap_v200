from rest_framework import serializers
from param.models import Param


class ComplectaionAllListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Param
        fields = ('id', 'title')
