from rest_framework import serializers
from param.models import Mark


class SubMarkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mark
        fields = ("id", "title")


class MarksSerializer(serializers.ModelSerializer):
    submodels = SubMarkSerializer(source='models', many=True, required=False)

    class Meta:
        model = Mark
        fields = ("id", "title", "submodels",)

