from django.contrib import admin
from generic.abstract.admin import AbstractImageAdmin
from ..models.include_area import IncludeArea


@admin.register(IncludeArea)
class IncludeAreaAdmin(AbstractImageAdmin):
    search_fields = ('title', 'code')
    list_display = ('thumb', 'title', 'is_show', 'code', 'sort')
    list_display_links = ('thumb', 'title')
    list_editable = ('is_show', 'sort')
    list_filter = ('is_show', 'code')

