from django_filters import rest_framework as filters
from ..models import Product


class ProductListFilter(filters.FilterSet):
    """
    API: Фильтр для списка товаров
    """
    ids = filters.CharFilter(method='get_ids', help_text='список ID товаров')

    def get_ids(self, queryset, name, value):
        """
        Получить список активный товаров по переданным ID
        GET: /api/products/?ids=1,2,3,4
        """
        if value is not None:
            # если нечаянно запрашивают список с запятой на конце
            if value[-1] in ', ':
                value = value[0:-1]

            value = [int(x) for x in value.split(',')]
            queryset = queryset.filter(pk__in=value)
        return queryset.all()
       
    class Meta:
        model = Product
        fields = ('ids',)

