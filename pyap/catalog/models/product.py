from decimal import Decimal
from django.db import models
from django.urls import reverse
from catalog.models import Catalog
from generic.abstract.models import AbstractPageSeoModel
from .product_image import ProductImage
from .product_item import ProductItem
from .product_comment import ProductComment


class Product(AbstractPageSeoModel):
    """
    Модель товара.
    """
    articul = models.CharField(max_length=256, verbose_name='Артикул (уник)', unique=True, blank=True, null=True)
    catalog = models.ManyToManyField(Catalog, blank=True, verbose_name='Категории')
    is_bestseller = models.BooleanField(default=False, verbose_name='Хит продаж')
    is_new = models.BooleanField(default=True, verbose_name='Новинка')
    recommend_products = models.ManyToManyField(
        'self', verbose_name='Рекомендованные/Похожие', blank=True, limit_choices_to={'is_show': True},
        help_text='Отображаются внизу карточки товара, как рекомендованные или похожие товары')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        # не отображать, если клонированный объект
        if 'CLONE' in self.title or 'CLONE' in self.slug:
            self.is_show = False
        super(Product, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        """Удаление объекта и связаных с ним изображений"""
        if ProductImage.objects.filter(product=self).first():
            [image.delete() for image in ProductImage.objects.filter(product=self)]
        if self.get_main_image():
            self.get_main_image().image.delete()
        super(Product, self).delete(*args, **kwargs)

    def get_absolute_url(self):
        catalog_slug = '#'
        if self.catalog.first():
            catalog_slug = self.catalog.first().slug
            return reverse('product_detail', args=[catalog_slug, self.slug])
        else:
            return catalog_slug

    def get_main_item(self):
        return ProductItem.objects.filter(product_id=self.id, is_main=True).first() or self.productitem_set.first()

    def get_price(self):
        price = 0
        if self.get_main_item():
            price = Decimal(self.get_main_item().price)
        return price

    def get_price_discount(self):
        price = 0
        if self.get_main_item():
            price = Decimal(self.get_main_item().price_discount)
        return price

    def get_images(self):
        return ProductImage.objects.filter(product_id=self.id)

    def get_comments(self):
        return ProductComment.objects.filter(product_id=self.id, is_show=True)

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'
        unique_together = ('title', 'slug')

