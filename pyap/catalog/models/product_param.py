from django.db import models
from param.mixin import AbstractParamValueModel
from catalog.models.product import Product


class ProductParam(AbstractParamValueModel):
    class Meta:
        verbose_name = 'Параметры товара'
        verbose_name_plural = 'Параметры товара'
        ordering = ('product', 'param')

    product = models.ForeignKey(Product, verbose_name='Товар', on_delete=models.CASCADE)

    def __str__(self):
        return '{}: {}'.format(str(self.product), str(self.param))
