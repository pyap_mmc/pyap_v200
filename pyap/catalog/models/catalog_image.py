from django.db import models
from generic.abstract.models import AbstractImageModel


class CatalogImage(AbstractImageModel):
    """
    Фотографии для раздела Каталог
    """
    catalog = models.ForeignKey('catalog.Catalog', verbose_name='Каталог', on_delete=models.CASCADE)

    def __str__(self):
        return self.catalog.title

    def save(self, *args, **kwargs):
        self.set_image_title(self.catalog)
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = 'Каталог:Фото'
        verbose_name_plural = 'Каталог:Фото'
