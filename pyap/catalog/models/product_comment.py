from django.db import models
from generic.abstract.models import AbstractCommentModel


class ProductComment(AbstractCommentModel):
    """
    Коментарии к товару `product:Product`
    """
    product = models.ForeignKey('catalog.Product', verbose_name='Товар', null=True, on_delete=models.SET_NULL)
