from decimal import Decimal
from django.db import models
from django.core.validators import MinValueValidator
from django.urls import reverse
from generic.abstract.models import AbstractCreatedModel
from utils.translit_field import translaton_field


class ProductItem(AbstractCreatedModel):
    """Вариант товара."""
    product = models.ForeignKey('catalog.Product', verbose_name='Главный товар', on_delete=models.CASCADE)
    name = models.CharField(max_length=255, verbose_name='Наименование', help_text='Прим:размерный ряд')
    articul = models.CharField(max_length=255, verbose_name='Артикул(уник.)', blank=True, null=True, unique=True)
    price = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name='Цена', default=0,
        validators=[MinValueValidator(Decimal('0'))])
    price_discount = models.DecimalField(
        max_digits=10, decimal_places=2, default=0, validators=[MinValueValidator(Decimal('0'))],
        verbose_name='Акционная цена', help_text='Если указана - станет ценой продажи товара.')
    price_purchase = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name='Цена закупки', default=0,
        validators=[MinValueValidator(Decimal('0'))])
    quantity = models.IntegerField(verbose_name='Кол-во', default=0)
    is_main = models.BooleanField(default=False, verbose_name='Главный')

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.set_main()
        if self.price_discount and not self.price:
            self.price = self.price_discount
        if not self.articul and self.name:
            self.articul = translaton_field(self.name)
        super(ProductItem, self).save(*args, **kwargs)

    def get_absolute_url(self):
        catalog_slug = '#'
        if self.product.catalog.first():
            catalog_slug = self.product.catalog.first().slug
            return reverse('product_detail', args=[catalog_slug, self.product.slug])
        else:
            return catalog_slug

    # ------------ SETTER HELPER -------------
    def set_main(self):
        """
        Установить вариант товара Главым
        главных товаров не может быть много. Он один!
        """
        if self.product.productitem_set.filter(is_main=True).count() >= 1:
            self.is_main = False
        elif self.product.productitem_set.filter(is_main=True).count() == 0:
            self.is_main = True

    def get_price(self):
        return self.price

    class Meta:
        unique_together = ('product', 'name')
        ordering = ('product', '-is_main', 'name')
        verbose_name = 'Вариант продукта'
        verbose_name_plural = 'Варианты продукта'

