from django.urls import reverse
from generic.abstract.models import AbstractMPTTPageSeoModel
from .catalog_image import CatalogImage


class Catalog(AbstractMPTTPageSeoModel):
    """
    Каталог. Поддерживает вложенность. Привязка товаров:model:`catalog.Product`.
    """
    
    class Meta:
        verbose_name = 'Каталог'
        verbose_name_plural = 'Каталог'

    def get_images(self):
        return CatalogImage.objects.filter(catalog_id=self.id)

    def get_absolute_url(self):
        kwargs = {'slug': self.slug}
        return reverse('catalog', kwargs=kwargs)


