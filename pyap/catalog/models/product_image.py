from django.db import models
from generic.abstract.models import AbstractImageModel


class ProductImage(AbstractImageModel):
    """Дополнительное изображние к товару"""
    product = models.ForeignKey('catalog.Product', verbose_name='Товар', on_delete=models.CASCADE)

    def __str__(self):
        return self.product.title

    def save(self, *args, **kwargs):
        self.set_image_title(self.product)
        super(ProductImage, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Товар:Фото'
        verbose_name_plural = 'Товар:Фото'


