from django.db import models
from filter.models import (StrValueParam, IntValueParam, BoolValueParam)
from ..models import Product


class ProductFilter(models.Model):
    class Meta:
        verbose_name = 'Фильтр товара'
        verbose_name_plural = 'Фильтры товаров'  
        
    product = models.OneToOneField(Product, verbose_name='Товар', on_delete=models.CASCADE)
    str_param = models.ManyToManyField(StrValueParam, verbose_name='Строковые параметры', blank=True)
    int_param = models.ManyToManyField(IntValueParam, verbose_name='Целочисленные параметры', blank=True)
    bool_param = models.ManyToManyField(BoolValueParam, verbose_name='Булевы параметры', blank=True)
    
    def __str__(self):
        return str(self.product)

