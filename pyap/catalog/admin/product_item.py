from django.contrib import admin, messages
from django.utils.html import format_html
from generic.abstract.admin import AbstractDefaultAdmin, AbstractCreatedAdmin
from ..models.product_item import ProductItem


@admin.register(ProductItem)
class ProductItemAdmin(AbstractDefaultAdmin, AbstractCreatedAdmin):
    raw_id_fields = ('product',)
    search_fields = ('articul', 'name', 'product__title')
    prepopulated_fields = {'articul': ('name',)}
    list_display = ('name', 'articul', 'product', 'price', 'quantity', 'price_discount', 'is_main')
    list_editable = ('price_discount', 'is_main')
    list_display_links = ('name', 'articul', 'product')
    list_filter = ('is_main', 'product__catalog') + AbstractCreatedAdmin.list_filter

    def clone_object(self, request, queryset):
        """Копирование(клонирование) выбранных объектов - action"""
        prefix = '-_CLONE'
        for obj in queryset:
            name = obj.name if obj.name else str(obj.id)
            articul = obj.articul if obj.articul else 'art_' + str(obj.id)
            clone = obj
            clone.name = name + prefix
            clone.articul = articul + prefix
            clone.id = None
            clone.is_main = False
            clone.save()
            messages.success(request, format_html('Склонирован <b>{}</b>', obj))
    clone_object.short_description = 'Клонировать'

