from django.contrib import admin
from generic.abstract.admin import AbstractMPTTPageSeoAdmin, AbstractImageInlineAdmin, fields_element
from ..models import Catalog, CatalogImage


class CatalogImageInline(AbstractImageInlineAdmin):
    model = CatalogImage


@admin.register(Catalog)
class CatalogAdmin(AbstractMPTTPageSeoAdmin):
    inlines = (CatalogImageInline,)

    fieldsets = (
        fields_element(('parent',)),
        AbstractMPTTPageSeoAdmin.fieldsets[0],
        AbstractMPTTPageSeoAdmin.fieldsets[2],
        AbstractMPTTPageSeoAdmin.fieldsets[3],
    )
