from django.contrib import admin
from generic.abstract.admin import AbstractCommentAdmin
from ..models.product_comment import ProductComment


@admin.register(ProductComment)
class ProductCommentAdmin(AbstractCommentAdmin):
    raw_id_fields = AbstractCommentAdmin.raw_id_fields + ('product',)
    list_display = AbstractCommentAdmin.list_display + ('product',)
    list_filter = AbstractCommentAdmin.list_filter + ('product',)
