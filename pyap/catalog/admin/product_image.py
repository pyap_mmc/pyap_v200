from django.contrib import admin
from generic.abstract.admin import AbstractDefaultAdmin, AbstractImageAdmin
from ..models.product_image import ProductImage


@admin.register(ProductImage)
class ProductImageAdmin(AbstractDefaultAdmin, AbstractImageAdmin):
    raw_id_fields = ('product',)
    list_filter = ('product',) + AbstractImageAdmin.list_filter
    list_display = ('product',) + AbstractImageAdmin.list_display



