from django.contrib import admin, messages
from django.utils.html import format_html
from generic.abstract.admin import AbstractDefaultAdmin, AbstractCreatedAdmin
from ..models import ProductParam


@admin.register(ProductParam)
class ProductParamAdmin(AbstractDefaultAdmin):
    raw_id_fields = ('product', 'param',)
    #
    # exclude = (
    #     'int_value',
    #     'decimal_value'
    # )
    # search_fields = ('number', 'name', 'promo__title')
    # prepopulated_fields = {'number': ('name',)}
    # list_display = ('name', 'number', 'promo', 'price', 'quantity', 'price_discount', 'is_main')
    # list_editable = ('price_discount', 'is_main')
    # list_display_links = ('name', 'number', 'promo')
    # list_filter = ('is_main', 'promo__catalog') + AbstractCreatedAdmin.list_filter

    # def clone_object(self, request, queryset):
    #     """Копирование(клонирование) выбранных объектов - action"""
    #     prefix = '-_CLONE'
    #     for obj in queryset:
    #         name = obj.name if obj.name else str(obj.id)
    #         number = obj.number if obj.number else 'art_' + str(obj.id)
    #         clone = obj
    #         clone.name = name + prefix
    #         clone.number = number + prefix
    #         clone.id = None
    #         clone.is_main = False
    #         clone.save()
    #         messages.success(request, format_html('Склонирован <b>{}</b>', obj))
    # clone_object.short_description = 'Клонировать'

