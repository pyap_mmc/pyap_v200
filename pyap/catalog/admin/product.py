from django.contrib import admin, messages
from django.utils.html import format_html
from generic.abstract.admin import (AbstractImageInlineAdmin, AbstractPageSeoAdmin,
                                    AbstractDefaultStackedInlineAdmin, AbstractDefaultTabularInlineAdmin,
                                    fields_element)
from ..models import Product, ProductImage, ProductItem, ProductParam


class ProductParamInline(AbstractDefaultTabularInlineAdmin):
    model = ProductParam
    suit_classes = 'suit-tab suit-tab-param'


class ProductImageInline(AbstractImageInlineAdmin):
    model = ProductImage


class ProductItemInline(AbstractDefaultStackedInlineAdmin):
    model = ProductItem
    suit_classes = 'suit-tab suit-tab-price'


# class ProductFilterInline(AbstractDefaultStackedInlineAdmin):
#     model = ProductFilter
#     suit_classes = 'suit-tab suit-tab-filter'
#     filter_horizontal = ('str_param', 'int_param', 'bool_param')


@admin.register(Product)
class ProductAdmin(AbstractPageSeoAdmin):
    inlines = (ProductItemInline, ProductImageInline, ProductParamInline)
    raw_id_fields = ('catalog',) + AbstractPageSeoAdmin.raw_id_fields
    readonly_fields = AbstractPageSeoAdmin.readonly_fields
    filter_horizontal = ('recommend_products',) + AbstractPageSeoAdmin.filter_horizontal
    list_filter = ('catalog',) + AbstractPageSeoAdmin.list_filter
    list_display = ('articul',) + AbstractPageSeoAdmin.list_display + ('is_bestseller', 'is_new')
    list_display_links = ('articul',) + AbstractPageSeoAdmin.list_display_links
    list_editable = AbstractPageSeoAdmin.list_editable + ('is_bestseller', 'is_new')

    fieldsets = (
        fields_element(('catalog', 'articul',  'is_bestseller', 'is_new', 'recommend_products')),
        AbstractPageSeoAdmin.fieldsets[0],
        AbstractPageSeoAdmin.fieldsets[2],
        AbstractPageSeoAdmin.fieldsets[3],
    )

    suit_form_tabs = (
        ('content', 'КОНТЕНТ'),
        ('price', 'ЦЕНЫ/ВАРИАНТЫ ТОВАРА'),
        ('fields', 'ПОЛЯ'),
        ('param', 'ПАРАМЕТРЫ'),
        ('seo', 'СЕО'),
        ('image', 'ФОТО'),
        ('info', 'ИНФО'),
    )

    def clone_object(self, request, queryset):
        """Копирование(клонирование) выбранных объектов - action"""
        for obj in queryset:
            prefix = '-_CLONE'
            catalogs = obj.catalog.all()
            clone = obj
            clone.title += prefix
            if clone.articul:
                clone.articul += prefix
            clone.slug += prefix
            clone.description += prefix
            clone.seo_title = clone.seo_description = clone.seo_keywords = ''
            clone.id = None
            clone.save()
            [clone.catalog.add(c) for c in catalogs]
            messages.success(request, format_html('Склонирован <b>{}</b>', obj))
    clone_object.short_description = 'Клонировать'

    def get_price(self, obj):
        return obj.get_price()
    get_price.short_description = 'Цена'

    # --------------------------------------------------------------------------------------------
    # # это что бы при добавлении/изменении рубрики список выбора из поля parent был в виде дерева
    # def formfield_for_foreignkey(self, db_field, request, **kwargs):
    #     field = super(ProductAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
    #     if db_field.title == 'parent':
    #         field.choices = [('', '---------')]
    #         for rubric in Catalog.objects.all():
    #             field.choices.append((rubric.pk, '+--'*(rubric.level) + rubric.title))
    #     return field
    # --------------------------------------------------------------------------------------------

    # def formfield_for_manytomany(self, db_field, request, **kwargs):
    #     if db_field.name in 'recommend_products':
    #
    #         print(self.get_queryset(request))
    #         current_obj = self.get_queryset(request)
    #         print(kwargs)
    #     return super(ProductAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)

