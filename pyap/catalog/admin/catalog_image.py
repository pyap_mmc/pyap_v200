from django.contrib import admin
from generic.abstract.admin import AbstractDefaultAdmin, AbstractImageAdmin
from ..models import CatalogImage


@admin.register(CatalogImage)
class CatalogImageAdmin(AbstractDefaultAdmin, AbstractImageAdmin):
    raw_id_fields = ('catalog',)
    list_filter = ('catalog',) + AbstractImageAdmin.list_filter
    list_display = ('catalog',) + AbstractImageAdmin.list_display
