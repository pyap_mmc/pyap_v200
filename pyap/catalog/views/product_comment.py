from django.shortcuts import get_object_or_404
from django.views.generic.base import TemplateView
from generic.mixins import MainPageMixin
from django.shortcuts import redirect
from django.contrib import messages
from ..models import Product, ProductComment
from ..forms.product_comment import CommentForm


class ProductCommentView(MainPageMixin, TemplateView):
    """
    Отзыв к товару
    """
    form_class = CommentForm
    model = ProductComment
    template_name = 'product/templates/product_detail.html'

    def post(self, request, *args, **kwargs):
        obj = get_object_or_404(Product, id=request.POST.get('product'))
        review_form = CommentForm(request.POST)
        if review_form.is_valid():
            review_form.save()
            messages.success(request, ':) Спасибо! Комментарий оставлен.')
        else:
            messages.error(request, '(: Произошла ошибка при отправке отзыва.')
        return redirect(obj.get_absolute_url())
