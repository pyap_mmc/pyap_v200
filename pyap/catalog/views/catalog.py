import json
from django.views.generic.base import TemplateView
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from generic.mixins import MainPageMixin
from utils.pagination import get_pagination
from utils.leftbar import get_leftbar
from utils.sort import (sort_by_params, get_filter, filter_by_param)
from catalog.models import Catalog, Product, ProductItem
# from ..models.product_filter import ProductFilter
# from ..forms.filter_product import ProductFilterForm


class CatalogView(MainPageMixin, TemplateView):
    template_name = 'catalog/templates/catalog.html'

    def get_context_data(self, **kwargs):
        context = super(CatalogView, self).get_context_data(**kwargs)

        context['object'] = Catalog.objects.get(slug=context['slug'], is_show=True)
        context['leftbar'] = get_leftbar(Catalog, context['object'])

        catalog_id = context['leftbar']['root_obj'].id
        context['current_mainmenu'] = context['mainmenu'].filter(catalog_id=catalog_id).first()

        context['objects'] = Product.objects.filter(catalog=context['object'], is_show=True).order_by('-id')
        context['filter'] = get_filter(self.request, context['objects'])
        context['objects'] = filter_by_param(self.request, context['objects'])
        context['objects'] = sort_by_params(self.request, context['objects'])
        context['objects'] = get_pagination(self.request, context['objects'])

        return context

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        """Добавить товар в корзину на странице - карточка товара - Ajax"""
        # одиночный товар или у товара есть позиции для выбора
        obj = ProductItem.objects.get(id=request.POST['product_id'])
        from ..views import ProductDetail
        product_detail = ProductDetail()
        product_detail.post(request)

        response_data = {
            'product_id': obj.id,
            'name': obj.name,
            'articul': obj.articul,
            'quantity': int(request.POST.get('quantity', 0)),
            'price': float(obj.price)
        }

        return HttpResponse(json.dumps(response_data), content_type="application/json")



