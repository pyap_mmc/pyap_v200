# from django import forms
# from django.forms import ModelForm, inlineformset_factory, formset_factory, widgets
#
#
# from ..models import (
#     Promo, PromoImage
# )
#
#
# class CreatePromoImageForm(ModelForm):
#
#     class Meta:
#         model = PromoImage
#         fields = ('image',)
#
#
# CreatePromoFormSet = inlineformset_factory(
#     Promo, PromoImage,
#     fields=('image',),
#     extra=3, max_num=25
# )
#
# CreatePromoImagesFormSet = formset_factory(
#     CreatePromoImageForm, CreatePromoFormSet,
#     extra=3, max_num=25)
