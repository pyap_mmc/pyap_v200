# from django import forms
# from django.forms import widgets
#
# from param.models import Mark
# from generic.widgets import (TextInputWidget, SelectWidget)
# from generic.choice import (CURRENCY_CHOICES, CURRENCY_AZN)
# from generic.forms import (CSS_CLASS_TEXT_INPUT, CSS_CLASS_TEXTAREA)
# from ..models import Promo
#
#
# class CreatePromoForm(forms.ModelForm):
#     class Meta:
#         model = Promo
#         fields = (
#             'title',
#             'mark',
#             'html',
#             'username',
#             'email',
#             'phone',
#             'price',
#             'currency_choices',
#         )
#
#     title = forms.CharField(label='Заголовок объявления', widget=TextInputWidget)
#     mark = forms.ModelChoiceField(label='Марка', queryset=Mark.objects.marks(), widget=SelectWidget)
#     model = forms.ModelChoiceField(
#         label='Модель', required=False,
#         queryset=Mark.objects.models(), widget=SelectWidget,
#     )
#
#     html = forms.CharField(
#         label=False,
#         required=False,
#         widget=widgets.Textarea(attrs={
#             'class': CSS_CLASS_TEXTAREA,
#             'cols': 30,
#             'rows': 5,
#             'placeholder': 'Дополнительное описание',
#         })
#     )
#
#     username = forms.CharField(label='Имя', widget=TextInputWidget)
#     email = forms.EmailField(
#         label='Эл. почта',
#         help_text='Эл. почта не показывается на сайте на него будет выслан пароль для входа в личный кабинет',
#         widget=widgets.EmailInput(attrs={
#             'class': CSS_CLASS_TEXT_INPUT,
#             'placeholder': 'Эл. почта*',
#         })
#     )
#     phone = forms.CharField(label='Телефонный номер', widget=TextInputWidget)
#     price = forms.DecimalField(
#         label='Цена',
#         widget=widgets.NumberInput(attrs={
#             'class': CSS_CLASS_TEXT_INPUT,
#             'placeholder': '150.000',
#         }),
#     )
#     currency_choices = forms.ChoiceField(
#         label='Валюта',
#         choices=CURRENCY_CHOICES,
#         initial=CURRENCY_AZN,
#         widget=SelectWidget,
#     )
#
#     def save(self, commit=True):
#         obj = super(CreatePromoForm, self).save(commit=False)
#         obj.mark = self.cleaned_data.get('model', '')
#
#         if commit:
#             obj.save()
#
#         return obj
#
#     # def clean(self):
#     #     data = self.cleaned_data['recipients']
#     #     if "fred@example.com" not in data:
#     #         raise forms.ValidationError("You have forgotten about Fred!")
#     #
#     #     # Always return a value to use as the new cleaned data, even if
#     #     # this method didn't change it.
#     #     return data
