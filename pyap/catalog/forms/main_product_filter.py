from param.abstract.forms import ParamFieldMixin
from param.models import Param, ParamSet


class MainPromoFilterForm(ParamFieldMixin):
    """
    Форма для Главной страницы.
    Параметры для фильтрации объявлений.
    """

    def __init__(self, *args, **kwargs):
        self.instance = kwargs.pop('instance', None)
        super(MainPromoFilterForm, self).__init__(*args, **kwargs)

        for param in ParamSet.promo_home_filter():
            # инициализация парамера(ов) и его(их) значений
            if param.values().exists():
                self.init_selected_field(param, required=False)
            # Одиночные Параметры не имеющие ParamValue
            else:
                if param.typeof == Param.STR:
                    self.init_one_str_field(param, required=False)
                elif param.typeof == Param.DECIMAL:
                    self.init_one_decimal_field(param, required=False)
                elif param.typeof == Param.INT:
                    self.init_one_decimal_field(param, required=False)
                elif param.typeof == Param.BOOL:
                    self.init_one_bool_field(param, required=False)
    #
    # # def save(self):
    # #     instance = self.instance
    # #
    # #     for key, val in self.cleaned_data.items():
    # #         pk = int(key.replace('param_', ''))
    # #         param = Param.objects.get(id=pk)
    # #
    # #         if param.typeof == Param.STR:
    # #             PromoParam.objects.create(promo=instance, param_id=pk, str_value=val)
    # #         elif param.typeof == Param.BOOL:
    # #             PromoParam.objects.create(promo=instance, param_id=pk, bool_value=val)
    # #         elif param.typeof == Param.DECIMAL:
    # #             PromoParam.objects.create(promo=instance, param_id=pk, decimal_value=val)
    # #         elif param.typeof == Param.INT:
    # #             PromoParam.objects.create(promo=instance, param_id=pk, int_value=val)
    # #
    # #     return instance
