# import django_filters
# from django import forms
# # from catalog.models import ProductFilter
# from filter.models import (StrValueParam, BoolValueParam, IntValueParam)
#
#
# spf = StrValueParam.objects.all()
# bpf = BoolValueParam.objects.all()
# ipf = IntValueParam.objects.all()
#
#
# css_class = 'nav nav-pills nav-stacked category-menu'
#
#
# class ProductFilterForm(django_filters.FilterSet):
#     str_param = django_filters.ModelMultipleChoiceFilter(
#         widget=forms.CheckboxSelectMultiple(attrs={'class': css_class}), queryset=spf)
#     bool_param = django_filters.ModelMultipleChoiceFilter(
#         widget=forms.CheckboxSelectMultiple(attrs={'class': css_class}), queryset=bpf)
#     int_param = django_filters.ModelMultipleChoiceFilter(
#         widget=forms.CheckboxSelectMultiple(attrs={'class': css_class}), queryset=ipf)
#
#     class Meta:
#         model = ProductFilter
#         fields = ['str_param', 'bool_param', 'int_param']