# from django.forms import widgets
# from param.abstract.forms import ParamFieldMixin
# from param.models import Param, ParamSet
# from promo.models import PromoParam
#
#
# class CreatePromoParamsForm(ParamFieldMixin):
#     """
#     Форма, Параметры для будущего созданого объявления.
#     """
#
#     def __init__(self, *args, **kwargs):
#         self.instance = kwargs.pop('instance', None)
#         super(CreatePromoParamsForm, self).__init__(*args, **kwargs)
#
#         for param in ParamSet.promo_create_auto():
#
#             # Параметры имеющие queryset ParamValue
#             if param.promo_auto_create_values().exists():
#                 self.init_selected_field(param)
#
#                 if Param.DECOR_COLOR == param.decor:
#                     self.fields['param_' + str(param.pk)].help_text = 'Выберите наиболее близкий цвет автомобиля'
#                     self.fields['param_' + str(param.pk)].widget = widgets.RadioSelect()
#
#             # Параметры не имеющие ParamValue
#             elif Param.DECOR_COLOR != param.decor:
#                 if param.typeof == Param.STR:
#                     self.init_one_str_field(param)
#                 elif param.typeof == Param.DECIMAL:
#                     self.init_one_decimal_field(param)
#                 elif param.typeof == Param.INT:
#                     self.init_one_int_field(param)
#                 elif param.typeof == Param.BOOL:
#                     self.init_one_bool_field(param)
#
#     def save(self):
#         instance = self.instance
#
#         for key, val in self.cleaned_data.items():
#             pk = int(key.replace('param_', ''))
#             param = Param.objects.get(id=pk)
#
#             if param.typeof == Param.STR:
#                 PromoParam.objects.create(promo=instance, param_id=pk, str_value=val)
#             elif param.typeof == Param.BOOL:
#                 PromoParam.objects.create(promo=instance, param_id=pk, bool_value=val)
#             elif param.typeof == Param.DECIMAL:
#                 PromoParam.objects.create(promo=instance, param_id=pk, decimal_value=val)
#             elif param.typeof == Param.INT:
#                 PromoParam.objects.create(promo=instance, param_id=pk, int_value=val)
#
#         return instance
