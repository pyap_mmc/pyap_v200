# Generated by Django 2.1 on 2018-08-23 18:49

from django.db import migrations, models
import django.db.models.deletion
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='catalog',
            options={},
        ),
        migrations.AddField(
            model_name='catalog',
            name='is_allow_comments',
            field=models.BooleanField(default=False, verbose_name='разрешить комментарии'),
        ),
        migrations.AlterField(
            model_name='catalog',
            name='description',
            field=models.TextField(blank=True, null=True, verbose_name='Описание'),
        ),
        migrations.AlterField(
            model_name='catalog',
            name='parent',
            field=mptt.fields.TreeForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='subitems', to='catalog.Catalog', verbose_name='Родительский объект'),
        ),
        migrations.AlterField(
            model_name='catalog',
            name='title',
            field=models.CharField(db_index=True, default='заголовок объекта', max_length=255, null=True, verbose_name='Заголовок'),
        ),
    ]
