from django.urls import path
from .views import CatalogView
from .views.product import ProductDetail
from .views.product_comment import ProductCommentView


urlpatterns = [
    path('<str:slug>/', CatalogView.as_view(), name='catalog'),
    path('<str:catalog>/<str:product>/', ProductDetail.as_view(), name='product_detail'),
    path('comment/product/<int:product_id>/', ProductCommentView.as_view(), name='product_comment'),
]

