# Установка проекта на новый сервер

    sudo apt-get update
    sudo apt-get -y upgrade 
    sudo apt-get install -y python3-pip

There are a few more packages and development tools to install to ensure that we have 
a robust set-up for our programming environment: 
    
    sudo apt-get install build-essential libssl-dev libffi-dev python-dev
    git clone https://pyap_mmc@bitbucket.org/pyap_mmc/pyap_v200.git


дальше все как в README.md


Поддомен / Значение 
---------------------
free2.pyap.ru / 95.213.236.77


Полность переносим / клонируем весь проект на сервак с правильными параметрами.



NGINX
------------
    sudo nano /etc/nginx/sites-available/****

```
server {
    listen 80;
    server_name free2.pyap.ru;
    charset     utf-8;

    access_log  /home/dev_nepaday/logs/nginx-access.log;
    error_log   /home/dev_nepaday/logs/nginx-error.log;

    client_max_body_size 350M;


    # обслуживание медиа файлов и статики
    location /media  {
        alias /home/dev_nepaday/theme/media;
    }

    location /static {
        alias   /home/dev_nepaday/theme/static;

        expires 30d;

        gzip             on;
        gzip_min_length  1000;
        gzip_types       text/plain application/xml application/x-javascript application/javascript text/css text/json;
        gzip_comp_level  6;

        access_log off;
    }

    location / {
        proxy_pass http://127.0.0.1:3200/;
        proxy_set_header Host $server_name;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_send_timeout 350s;
        proxy_read_timeout 350s;
    }
}
```

    sudo ln -s /etc/nginx/sites-available/dev_nepaday etc/nginx/sites-enabled/ 
    
    
    sudo chown -R $USER:$USER /home/dev_nepaday
    sudo chmod -R 755 /home/dev_nepaday/
    sudo chmod -R 777 /home/dev_nepaday/pyap/pyap_v100.sqlite3 



Supervisor configuration
Create a supervisor configuration file: (Warning: Please note the .conf extension)
--------------------------------------------------------------------------------
sudo vim /etc/supervisor/conf.d/dev_nepaday.conf  

[program:dev_nepaday]
command=/home/dev_nepaday/venv/bin/gunicorn pyap.wsgi:application -c /home/dev_nepaday/pyap/pyap/gunicorn.conf.py
directory=/home/dev_nepaday/pyap
user=nobody
autorestart=true
redirect_stderr=true



sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl start dev_nepaday

Ждем когда встанут ДНС!!!