    git clone https://pyap_mmc@bitbucket.org/pyap_mmc/pyap_v200.git
    cd pyap_v200
    virtualenv --python=python3 venv
	source venv/bin/activate
	cd pyap/
	pip install -r requirements.txt
	python manage.py collectstatic --noinput
	python manage.py makemigrations
	python manage.py migrate
	python manage.py runserver
	
#  Развертывание проекта 

`pyap_v200` - название проекта используется как пример


Скачать или клонировать себе проект ``git clone https://pyap_mmc@bitbucket.org/pyap_mmc/pyap_v200.git``
Подготовка папок для проекта
    
    cd ~/projects
    mkdir pyap_v200
    cd pyap_v200
    
проверяем `ls`. Должны быть следующие директории 
* app 
* pyap 
* theme


Ставим виртуальное окружение. Активируем.

	virtualenv --python=python3 venv
	source venv/bin/activate


Устанавливаем Джанго и все необходимые зависимости. Входим в папку где лежит файл `manage.py`.
	
	 cd ~/projects/pyap_v200/pyap/
	 pip install -r requirements.txt


По умолчанию установливается база `SQLite` `pyap_v200` в проект 
	
	python manage.py collectstatic --noinput
	python manage.py makemigrations
	python manage.py migrate

Проект создан! Проверяем `python manage.py runserver`. 
Запускаем локальный сервер и переходим на страницу в `http://localhost:8000/`. 
	

[I]Заполним проект дефолтными данными

	python manage.py createsuperuser
 		name: `superuser`
 		emal: `superuser@mail.ru`
 		password: `superuser888`


Проверяем `python manage.py runserver`.


Создадим базу данных `xxx` и настроем `postgres` для проекта

	sudo apt-get update
	sudo apt-get install python3-pip python3-dev libpq-dev postgresql postgresql-contrib 
	
	# Если нужен `postgis`
	# sudo apt-get install postgis gdal-bin	

	sudo -u postgres psql
	postgres=# 
		CREATE DATABASE xxx;
		CREATE USER xxx WITH PASSWORD 'xxx';
		ALTER ROLE xxx SET client_encoding TO 'utf8';
		ALTER ROLE xxx SET default_transaction_isolation TO 'read committed';
		ALTER ROLE xxx SET timezone TO 'UTC';
		GRANT ALL PRIVILEGES ON DATABASE xxx TO xxx;
		\q


Меняем настройку подключения к Базе в `~/projects/pyap_v200/pyap/pyap/settings.py`
```
...
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'xxx',
        'USER': 'xxx',
        'PASSWORD': 'xxx',
        'HOST': 'localhost',
        'PORT': '',
    }
}
...
```
	
	python manage.py migrate


[I]Заполним проект дефолтными данными
	
	# ... создаем суперпользователя и приминяем фикстуры ...


Проверяем `python manage.py runserver`.

# Если сервер


## GIT / BITBACKET 
	
	cd ~/projects/pyap_v200

	git config --global user.name "Mihail"
	git config --global user.email pyapmail@gmail.com
	git config --global core.pager 'less -r'

	git init
	git add *
	git commit -m 'Hello World!'
	

#### TRY/EXCEPT 

1.`Error: That port is already in use.`
	
	fuser -k 8000/tcp 


# ----      ---- #
  --- [pYAp] --- 
# ----      ---- #



