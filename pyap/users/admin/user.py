from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as DefaultUserAdmin
from daterange_filter.filter import DateRangeFilter
from generic.abstract.admin import AbstractDefaultAdmin
from ..forms.user_change_form import UserChangeForm
from ..models.user_profile import UserProfile
from ..models.user_link import UserLink

admin.site.unregister(User)


class UserProfileInline(admin.StackedInline):
    verbose_name_plural = 'Профиль'
    model = UserProfile


class UserLinkInline(admin.TabularInline):
    verbose_name = 'Ссылка'
    verbose_name_plural = 'Пользовательские ссылки'
    model = UserLink
    extra = 0
    classes = ('collapse',)


@admin.register(User)
class UserAdmin(DefaultUserAdmin, AbstractDefaultAdmin):
    inlines = (UserProfileInline, UserLinkInline)
    form = UserChangeForm
    date_hierarchy = 'date_joined'
    readonly_fields = ('date_joined', 'last_login', 'password')
    search_fields = ('username', 'email', 'last_name')
    list_filter = (
        'is_active', 'is_staff', 'is_superuser',
        'date_joined', 'last_login',
    )
    list_display = ('email', 'last_name', 'is_active', 'is_staff', 'is_superuser')
    filter_horizontal = ('groups', 'user_permissions')

    # TODO: [fieldsets admin]
    # fieldsets = (
    #     ('Основные данные', {
    #         'fields': (
    #             'username', 'first_name', 'last_name', 'email',
    #             ('is_active', 'is_staff', 'is_superuser')
    #         )
    #     }),
    #     ('Дополнительно', {
    #         'fields': ('groups', 'user_permissions', 'date_joined', 'last_login'),
    #         'classes': ('collapse',)
    #     }),
    #
    # )
