from django.contrib import admin
from django.contrib.auth.models import Group
from utils.abstract_admin import DefaultSettings

admin.site.unregister(Group)


@admin.register(Group)
class GroupAdmin(DefaultSettings):
    menu_title = "Группы"
    menu_group = "Пользователи"

    search_fields = ('name', 'permissions__name')
    list_filter = ('permissions', )
    filter_horizontal = ('permissions',)
