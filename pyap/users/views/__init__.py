from .login import logout_view, Login
from .register import Register
from .profile import Profile
from .review import ReviewProducts
from .orders import Orders
from .order_detail import OrderDetail

