from django.contrib import admin
from generic.abstract.admin import (AbstractPageSeoAdmin, AbstractImageInlineAdmin)
from ..models import (Page, PageImage)


class PageImageInline(AbstractImageInlineAdmin):
    model = PageImage


@admin.register(Page)
class PageAdmin(AbstractPageSeoAdmin):
    inlines = (PageImageInline,)
    fieldsets = (
        AbstractPageSeoAdmin.fieldsets[0],
        AbstractPageSeoAdmin.fieldsets[2],
        AbstractPageSeoAdmin.fieldsets[3],
    )
