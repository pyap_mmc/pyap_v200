from django.contrib import admin
from generic.abstract.admin import AbstractDefaultAdmin, AbstractImageAdmin
from ..models.page_image import PageImage


@admin.register(PageImage)
class PageImageAdmin(AbstractDefaultAdmin, AbstractImageAdmin):
    raw_id_fields = ('page',)
    search_fields = ('image_title', 'page__title')
    list_display = AbstractImageAdmin.list_display + ('page',)
