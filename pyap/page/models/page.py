from django.urls import reverse
from generic.abstract.models import AbstractPageSeoModel
from .page_image import PageImage
from .page_comment import PageComment


class Page(AbstractPageSeoModel):
    """
    Модель для контент-страниц, таких как О нас, Контакты...
    """

    class Meta:
        verbose_name = 'страница'
        verbose_name_plural = 'страницы'
        abstract = False

    def get_absolute_url(self):
        return reverse('page_detail', args=[self.slug])

    def get_comments(self):
        qs = PageComment.objects.filter(page_id=self.id)
        return qs

    def get_images(self):
        return PageImage.objects.filter(page_id=self.id)
