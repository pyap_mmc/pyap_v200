from django.db import models
from generic.abstract.models import AbstractCommentModel


class PageComment(AbstractCommentModel):
    """
    Коментарии к контент-странице `page.Page`
    """
    page = models.ForeignKey('page.Page', verbose_name='Страница', null=True, on_delete=models.SET_NULL)
