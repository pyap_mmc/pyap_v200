from django.db import models
from generic.abstract.models import AbstractImageModel


class PageImage(AbstractImageModel):
    """
    Фотографии для Страницы :model:`page.Page`.
    """
    page = models.ForeignKey('page.Page', verbose_name='Страница', on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.set_image_title(self.page)
        super().save(*args, **kwargs)
