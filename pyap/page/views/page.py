from django.views.generic.base import TemplateView
from django.shortcuts import get_object_or_404
from generic.mixins import MainPageMixin
from utils.leftbar import get_leftbar
from blog.models import Blog
from ..models.page import Page
from ..forms.page_comment import CommentForm


class PageView(MainPageMixin, TemplateView):
    template_name = 'page/templates/page.html'

    def get_context_data(self, **kwargs):
        context = super(PageView, self).get_context_data(**kwargs)
        context['object'] = get_object_or_404(Page, slug=context['slug'], is_show=True)
        initial = {
            'obj': context['object'],
            'request': self.request,
        }
        comment_form = CommentForm(initial=initial)
        context['comment_form'] = comment_form
        context['leftbar'] = get_leftbar(Blog, Blog.objects.first())
        return context
