from django.views.generic.base import TemplateView
from django.shortcuts import get_object_or_404, redirect
from django.contrib import messages
from generic.mixins import MainPageMixin
from ..models.page import Page
from ..models.page_comment import PageComment
from ..forms.page_comment import CommentForm


class PageCommentView(MainPageMixin, TemplateView):
    """
    Коментарий к странице
    """
    model = PageComment
    form_class = CommentForm
    template_name = 'page/templates/page.html'

    def post(self, request, *args, **kwargs):
        obj = get_object_or_404(Page, id=request.POST.get('page'))
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            comment_form.save()
            messages.success(request, ':) Спасибо! Отзыв принят.')
        else:
            messages.error(request, '(: Произошла ошибка при отправке отзыва.')
        return redirect(obj.get_absolute_url())
