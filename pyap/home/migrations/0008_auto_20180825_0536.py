# Generated by Django 2.1 on 2018-08-25 05:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0007_home_is_allow_comments'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='homeimage',
            unique_together=set(),
        ),
    ]
