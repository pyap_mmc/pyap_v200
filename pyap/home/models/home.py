from django.db import models
from generic.abstract.models import AbstractPageSeoModel
from blog.models import Blog
from .home_image import HomeImage


class Home(AbstractPageSeoModel):
    """
    Настройки главной страницы
    """
    class Meta:
        verbose_name = 'главная страница'
        verbose_name_plural = 'главная страница'

    blog = models.ForeignKey(
        Blog, verbose_name='Блог', blank=True, null=True,
        help_text='Например, создать блог ПОСЛЕДНЕИ НОВОСТИ и закрепить его на главную страницу.'
                  'Или выбрать из существующих', on_delete=models.CASCADE)

    def get_images(self):
        return HomeImage.objects.filter(home_id=self.id)



