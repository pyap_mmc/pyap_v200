from django.db import models
from generic.abstract.models import AbstractImageModel


class HomeImage(AbstractImageModel):
    """
    Фотографии для Главной страницы
    """
    home = models.ForeignKey('home.Home', verbose_name='Главная страница', on_delete=models.CASCADE)

    def __str__(self):
        return self.home.title

    def save(self, *args, **kwargs):
        self.set_image_title(self.home)
        super().save(*args, **kwargs)
