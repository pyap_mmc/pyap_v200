from django.contrib import admin
from generic.abstract.admin import (AbstractPageSeoAdmin, AbstractImageInlineAdmin, fields_element)
from ..models.home import (Home, HomeImage)


class HomeImageInline(AbstractImageInlineAdmin):
    model = HomeImage


@admin.register(Home)
class HomeAdmin(AbstractPageSeoAdmin):
    inlines = (HomeImageInline,)
    raw_id_fields = AbstractPageSeoAdmin.raw_id_fields + ('blog',)
    list_filter = AbstractPageSeoAdmin.list_filter + ('blog',)

    fieldsets = (
        fields_element(('blog',)),              # fields
        AbstractPageSeoAdmin.fieldsets[0],      # content
        AbstractPageSeoAdmin.fieldsets[2],      # seo
        AbstractPageSeoAdmin.fieldsets[3],      # info + ...inline
    )



