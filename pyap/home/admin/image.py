from django.contrib import admin
from generic.abstract.admin import AbstractDefaultAdmin, AbstractImageAdmin
from ..models.home import HomeImage


@admin.register(HomeImage)
class HomeImageAdmin(AbstractDefaultAdmin, AbstractImageAdmin):
    pass
