from django.contrib import admin
from generic.abstract.admin import AbstractImageAdmin, AbstractDefaultAdmin
from ..models.slider import SliderHome


@admin.register(SliderHome)
class SliderHomeAdmin(AbstractDefaultAdmin, AbstractImageAdmin):
    search_fields = ('title',)
    list_display = ('thumb', 'title', 'description', 'url', 'sort')
    list_display_links = ('thumb', 'title')
    list_editable = ('sort',)
    list_filter = ('sort', )
