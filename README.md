### Быстрая установка .

    git clone https://pyap_mmc@bitbucket.org/pyap_mmc/pyap_v200.git
    cd pyap_v200
    virtualenv -p python3 venv
    source venv/bin/activate
    
    cd pyap
    pip install -r requirements.txt
    python manage.py runserver 

[Смотрим](http://localhost:8000). 
